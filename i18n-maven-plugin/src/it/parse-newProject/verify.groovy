/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

def checkKeysInFile(path, keys) {

    file = new File(basedir, path);
    if (!file.exists()) {
        println("Could not find file [" + file + "]");
        return false;
    }

    content = file.text;

    for (key in keys) {
        if (!content.contains(key)) {
            println("Could not find " + key + " in file " + file);
            return false;
        }
    }
    return true;
}

def checkKeysNotInFile(path, keys) {

    file = new File(basedir, path);
    if (!file.exists()) {
        println("Could not find file [" + file + "]");
        return false;
    }

    content = file.text;

    for (key in keys) {
        if (content.contains(key)) {
            println("Should not find " + key + " in file " + file);
            return false;
        }
    }
    return true;
}

assert checkKeysInFile('target/i18n/getters/java.getter',
        ['javaGetter.key1', 'javaGetter.key2', 'javaGetter.key3'])

return true;
