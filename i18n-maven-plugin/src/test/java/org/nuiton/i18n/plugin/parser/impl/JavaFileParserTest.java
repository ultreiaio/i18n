/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.parser.impl;

import org.apache.maven.plugin.logging.SystemStreamLog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashSet;
import java.util.Set;

/**
 * Tests the class {@link ParseJavaMojo.JavaFileParser}
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.3
 */
public class JavaFileParserTest {


    private static final Charset ENCODING = StandardCharsets.UTF_8;

    private static final SystemStreamLog log = new SystemStreamLog();

    private ParseJavaMojo.JavaFileParser parser;

    private Set<String> detectedKeys;
    private Path temporaryPath;

    @Before
    public void setUp() {
        SortedProperties oldParser = new SortedProperties(ENCODING.name());


        parser = new ParseJavaMojo.JavaFileParser(log, ENCODING, oldParser, null) {
            @Override
            protected void registerKey(File file, String key) {
                detectedKeys.add(key);
            }
        };
        detectedKeys = new HashSet<>();
        temporaryPath = Path.of(System.getProperty("java.io.tmpdir"));
    }

    @After
    public void tearDown() {
        detectedKeys.clear();
        detectedKeys = null;
        parser = null;
    }

    @Test
    public void getKeys() throws IOException {

        String classContent = "" +
                "package org.nuiton.i18n;" +
                "import io.ultreia.java4all.i18n.I18n;" +
                "import static io.ultreia.java4all.i18n.I18n.t;" +
                "class TestFuctionN {" +
                "String key = I18n.t(\"a\");" +
                "String key2 = io.ultreia.java4all.i18n.I18n.t(\"b\");" +
                "String key3 = t(\"c\");" +
                "String key4 = t(" +
                "\"d\");" +
                "String key5 = tt(\"e\");" +
                "String t(String n) { return null; }" +
                "}";

        File file = temporaryPath.resolve("getKeys").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{"e"}, "a", "b", "c", "d");
    }

    @Test
    public void getKeys2() throws IOException {

        String classContent = "" +
                "package org.nuiton.i18n;" +
                "import io.ultreia.java4all.i18n.I18n;" +
                "import static io.ultreia.java4all.i18n.I18n.n;" +
                "class TestFuctionN {" +
                "String key = I18n.n(\"a\");" +
                "String key2 = io.ultreia.java4all.i18n.I18n.n(\"b\");" +
                "String key3 = n(\"c\");" +
                "String key4 = n(" +
                "\"d\");" +
                "String key5 = nn(\"e\");" +
                "String nn(String n) { return null; }" +
                "}";

        File file = temporaryPath.resolve("getKeys2").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{"e"}, "a", "b", "c", "d");
    }

    @Test
    public void getKeys3() throws IOException {

        String classContent = "" +
                "package org.nuiton.i18n;" +
                "import io.ultreia.java4all.i18n.I18n;" +
                "import java.util.Locale;" +
                "import static io.ultreia.java4all.i18n.I18n.l;" +
                "import static java.util.Locale.UK;" +
                "class TestFuctionN {" +
                "String key = I18n.l(Locale.UK, \"a\");" +
                "/*FIXME Can't detect this String key = I18n.l(UK, \"aa\");*/" +
                "String key2 = io.ultreia.java4all.i18n.I18n.l(Locale.UK, \"b\");" +
                "String key3 = l(Locale.UK, \"c\");" +
                "String key4 = l(Locale.UK, " +
                "\"d\");" +
                "String key5 = ll(java.util.Locale.UK, \"e\");" +
                "String t(String n) { return null; }" +
                "}";

        File file = temporaryPath.resolve("getKeys3").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{"e", "aa"}, "a", "b", "c", "d");
    }

    /**
     * This is a special case for non affectation on "l(Locale.FRENCH, \"javaGetter.key3\");".
     *
     * @throws IOException if any ?
     */
    @Test
    public void getKeys4() throws IOException {

        String classContent = "" +
                "package org.nuiton.i18n.test;" +

                "import java.util.Locale;" +

                "import static io.ultreia.java4all.i18n.I18n.t;" +
                "import static io.ultreia.java4all.i18n.I18n.n;" +
                "import static io.ultreia.java4all.i18n.I18n.l;" +

                "public class MyBean {" +

                "    protected String field1 = n(\"javaGetter.key1\");" +

                "    protected String field2 = t(\"javaGetter.key2\");" +

                "    public void method() {" +
                "        l(Locale.FRENCH, \"javaGetter.key3\");" +
                "    }" +
                "}";

        File file = temporaryPath.resolve("getKeys4").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{}, "javaGetter.key1", "javaGetter.key3", "javaGetter.key3");
    }

    /**
     * Test que la synthax java 8 est supportée.
     *
     * @throws IOException if any ?
     */
    @Test
    public void getJava8Keys() throws IOException {
        String classContent = "" +
                "package org.nuiton.i18n;" +
                "import io.ultreia.java4all.i18n.I18n;" +
                "class TestFuction8 {" +
                "   public static void main() {" +
                "       List<String> test = new ArrayList<>();" +
                "       test.removeIf(s -> s.isEmpty());" +
                "       test.forEach(s -> t(s));" +
                "       test.forEach((s) -> t(\"new string found\", s));" +
                "       test.forEach(s -> t(\"new string found 2\"));" +
                "" +
                "       Consumer<String> translator = s -> t(\"new string in consumer\");" +
                "       test.forEach(translator);" +
                "   }" +
                "}";

        File file = temporaryPath.resolve("getJava8Keys").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{"s"}, "new string found", "new string found 2", "new string in consumer");
    }

    /**
     * Test que l'enchainement de deux appels t(t("")) est bien détecté.
     *
     * @throws IOException if any ?
     */
    @Test
    public void getDoubleTKeys() throws IOException {
        String classContent = "" +
                "package org.nuiton.i18n;" +
                "import io.ultreia.java4all.i18n.I18n;" +
                "class TestFuction8 {" +
                "   public static void main() {" +
                "       System.outputDirectory.println(t(t(\"test deux t\"), \"no\"));" +
                "   }" +
                "}";

        File file = temporaryPath.resolve("getDoubleTKeys").toFile();
        Files.write(file.toPath(), classContent.getBytes());

        parseLine(file, new String[]{"no"}, "test deux t");
    }

    private void parseLine(File f, String[] notExpectedKeys, String... expectedKeys) throws IOException {
        parser.parseFile(f);

        for (String expectedKey : expectedKeys) {

            Assert.assertTrue("Key " + expectedKey +
                                      " was expected from file " + f,
                              detectedKeys.contains(expectedKey));
        }

        for (String expectedKey : notExpectedKeys) {

            Assert.assertFalse("Key " + expectedKey +
                                       " was NOT expected from file " + f,
                               detectedKeys.contains(expectedKey));
        }
    }

}
