/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.bundle;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests the {@link StringFormatToMessageFormatConverter}.
 *
 * Created: 05/05/11
 *
 * @author Florian Desbois
 */
public class StringFormatToMessageFormatConverterTest {

    private BundleFormatConverter converter;

    @Test
    public void testConvert() throws Exception {


        converter = new StringFormatToMessageFormatConverter();

        convertAndAssertEquals("Salut à tous pour l'ouverture",
                               "Salut à tous pour l''ouverture");

        convertAndAssertEquals("Salut à tous pour l''ouverture",
                               "Salut à tous pour l''''ouverture");

        convertAndAssertEquals("Salut à tous pour l'ouverture de %s",
                               "Salut à tous pour l''ouverture de {0}");

        convertAndAssertEquals("Salut à tous pour l'ouverture de %$1s",
                               "Salut à tous pour l''ouverture de {0}");

        convertAndAssertEquals("Salut à tous pour l'ouverture de %$2s le %$1t",
                               "Salut à tous pour l''ouverture de {1} le {0}"
        );

        convertAndAssertEquals(
                "Salut à tous pour l'ouverture de %$2s le %$1td à l'heure %1tH",
                "Salut à tous pour l''ouverture de {1} le {0} à l''heure {0}"
        );

        convertAndAssertEquals("Salut à tous pour l''ouverture de '%$1s'",
                               "Salut à tous pour l''''ouverture de ''{0}''");
    }

    protected void convertAndAssertEquals(String incoming, String expected) {
        String actual = converter.convert(incoming);
        Assert.assertEquals(expected, actual);
    }
}
