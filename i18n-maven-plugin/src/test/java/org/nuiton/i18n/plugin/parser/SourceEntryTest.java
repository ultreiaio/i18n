package org.nuiton.i18n.plugin.parser;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;
import org.nuiton.i18n.plugin.parser.impl.ParseJavaMojo;

import java.io.File;

/**
 * Tests the {@link SourceEntry}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4.3
 */
public class SourceEntryTest {

    @Test
    public void testGetIncludedFiles() {
        SourceEntry entry = new SourceEntry();
        File basedir = new File(".");

        File defaultBasedir = new File(basedir, "src" + File.separator + "test" + File.separator + "java");
        File defaultBasedir2 = new File(basedir, "src" + File.separator + "test" + File.separator + "resources");

        String[] includedFiles;

        includedFiles = entry.getIncludedFiles(defaultBasedir,
                                               new String[]{ParseJavaMojo.DEFAULT_INCLUDES},
                                               I18nSourceEntry.EMPTY_STRING_ARRAY);
        Assert.assertNotNull(includedFiles);
        Assert.assertTrue(includedFiles.length > 0);

    }
}
