/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import org.apache.maven.plugin.logging.Log;
import org.codehaus.plexus.util.DirectoryScanner;
import org.nuiton.io.FileUpdater;

import java.io.File;
import java.lang.annotation.Annotation;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A simple model for a sourceEntry represents by a basedir and includes and/or
 * exlucdes pattern.
 *
 * The class offers the usefull method : {@link #getIncludedFiles(File,
 * String[], String[])}
 *
 * to obtain the list of files from the {@link #basedir} directory which
 * respects the {@link #includes} and/or {@link #excludes} patterns using an
 * internal  {@link DirectoryScanner} object.
 *
 * Note : <b>The class does not extends <code>DirectoryScanner</code> since we
 * DO not want to expose his methods.</b>
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class SourceEntry {

    /**
     * If you want to restrict use of the entry, set the class name goal to this
     * property via {@link #setSpecificGoal(String)}.
     *
     * If let to <code>null</code>, all goals can use this entry.
     */
    protected String specificGoal;

    protected File basedir;

    protected String[] includes;

    protected String[] excludes;

    /** Files to be find */
    protected String[] files;

    protected String[] skipFiles;

    protected String skipMessage;

    protected FileUpdater updater;

    public String[] getExcludes() {
        return excludes;
    }

    public void setExcludes(String[] excludes) {
        this.excludes = excludes;
    }

    public String[] getIncludes() {
        return includes;
    }

    public void setIncludes(String[] includes) {
        this.includes = includes;
    }

    public File getBasedir() {
        return basedir;
    }

    public void setBasedir(File basedir) {
        this.basedir = basedir;
    }

    public String getSpecificGoal() {
        return specificGoal;
    }

    public void setSpecificGoal(String specificGoal) {
        this.specificGoal = specificGoal;
    }

    public boolean useForGoal(String goal) {
        return specificGoal == null || specificGoal.equalsIgnoreCase(goal);
    }

    public boolean hasSrc() {
        return basedir != null;
    }

    public boolean hasIncludes() {
        return includes != null && includes.length > 0;
    }

    public boolean hasExcludes() {
        return excludes != null && excludes.length > 0;
    }

    /**
     * Test if a file is up to date and not to be treated.
     *
     *
     * @param file the file path to test
     * @return {@code true} if file is up to date and do not need to be parsed
     * @see FileUpdater
     */
    public final boolean isFileUptodate(File file) {
        return updater != null && updater.isFileUpToDate(file);
    }

    public String[] getIncludedFiles(File defaultBasedir,
                                     String[] defaultIncludes,
                                     String[] defaultExcludes) {
        // normalized entry
        if (!hasSrc()) {
            setBasedir(defaultBasedir);
        }
        if (!hasIncludes()) {
            setIncludes(defaultIncludes);
        }
        if (!hasExcludes()) {
            setExcludes(defaultExcludes);
        }
        // init directory scanner
        DirectoryScanner ds = new DirectoryScanner();
        ds.setBasedir(getBasedir());
        ds.setIncludes(getIncludes());
        if (hasExcludes()) {
            ds.setExcludes(getExcludes());
        }
        // scan
        ds.scan();
        // get found files
        String[] foundFiles;
        foundFiles = ds.getIncludedFiles();
        return foundFiles;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("basedir:").append(basedir);
        if (includes != null) {
            sb.append(", includes:").append(Arrays.toString(includes));
        }
        if (excludes != null && excludes.length > 0) {
            sb.append(", excludes:").append(Arrays.toString(excludes));
        }
        return sb.toString();
    }

    public String[] getIncludedFiles(File basedir,
                                     String[] defaultIncludes,
                                     String[] defaultExcludes,
                                     URLClassLoader loader,
                                     List<String> annotationClass,
                                     Log log) {
        List<String> result = new ArrayList<>();

        for (String s :
                getIncludedFiles(basedir, defaultIncludes, defaultExcludes)) {
            if (filterByAnnotation(s, loader, annotationClass, log)) {
                result.add(s);
            }
        }
        return result.toArray(new String[result.size()]);
    }

    protected boolean filterByAnnotation(String file,
                                         URLClassLoader loader,
                                         List<String> annotationClass,
                                         Log log) {


        Annotation annotation = getAnnotation(file, loader, annotationClass,
                                              log);

        boolean result = annotation != null;

        if (result && log.isDebugEnabled()) {
            log.debug("find i18n annotated file  : " + file);
        }
        return result;
    }

    protected String getFQN(String file) {
        String filePath = file;
        filePath = filePath.substring(0, filePath.length() - ".java".length());
        String replaceEx = File.separator.equals("\\") ? "\\\\" : File.separator;
        return filePath.replaceAll(replaceEx, ".");
    }

    public Class<?> getClass(String file, URLClassLoader loader, Log log) {
        String fqn = getFQN(file);
        try {
            return loader.loadClass(fqn);

        } catch (Throwable e) {
            log.warn("could not find class " + fqn + " " + e);
            return null;
        }
    }

    public Annotation getAnnotation(String file,
                                    URLClassLoader loader,
                                    List<String> annotationClass,
                                    Log log) {

        Class<?> currentClass = getClass(file, loader, log);

        try {
            Annotation[] annos = currentClass.getAnnotations();
            if (annos != null && annos.length > 0) {
                for (Annotation anno : annos) {
                    if (annotationClass.contains(
                            anno.annotationType().getName())) {
                        return anno;
                    }
                }
            }
        } catch (Throwable e) {
            log.warn("could not find annotation for " + file + " " + e);
        }
        return null;

    }

    public Class<?> getClass(File file, URLClassLoader loader, Log log) {
        String f = file.getAbsolutePath().substring(
                basedir.getAbsolutePath().length() + 1);
        return getClass(f, loader, log);
    }

    public Annotation getAnnotation(File file,
                                    URLClassLoader loader,
                                    List<String> annotationClass,
                                    Log log) {
        String f = file.getAbsolutePath().substring(
                basedir.getAbsolutePath().length() + 1);
        return getAnnotation(f, loader, annotationClass, log);
    }

    public String getSkipMessage() {
        return skipMessage;
    }

    public Set<File> getFiles() {
        return Arrays.stream(files).map(f-> new File(basedir.getAbsolutePath() ,f)).collect(Collectors.toSet());
    }

    public String[] getSkipFiles() {
        return skipFiles;
    }

    public int getFoudFiles() {
        return skipFiles.length + files.length;
    }

    public FileUpdater getUpdater() {
        return updater;
    }

    public void setUpdater(FileUpdater updater) {
        this.updater = updater;
    }
}
