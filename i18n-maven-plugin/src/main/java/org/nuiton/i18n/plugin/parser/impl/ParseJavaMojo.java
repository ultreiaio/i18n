/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser.impl;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.nuiton.i18n.plugin.parser.AbstractFileParser;
import org.nuiton.i18n.plugin.parser.FileParser;
import org.nuiton.i18n.plugin.parser.I18nParseMojoSupport;
import org.nuiton.i18n.plugin.parser.I18nSourceEntry;
import org.nuiton.i18n.plugin.parser.ParserException;
import org.nuiton.i18n.plugin.parser.SourceEntry;
import org.nuiton.i18n.plugin.parser.java.Java8BaseVisitor;
import org.nuiton.i18n.plugin.parser.java.Java8Lexer;
import org.nuiton.i18n.plugin.parser.java.Java8Parser;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.ArgumentListContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.ExpressionContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodInvocationContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodInvocation_lfno_primaryContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.MethodNameContext;
import org.nuiton.i18n.plugin.parser.java.Java8Parser.TypeNameContext;
import org.nuiton.io.FileUpdater;
import org.nuiton.io.FileUpdaterHelper;
import org.nuiton.io.SortedProperties;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Parse java files to detect new i18n keys.
 * <p>
 * <b>Note: </b> this goal must always be invoked before the {@code process-resources}
 * phase, otherwise all files will be considered as uptodate.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "parse-java", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class ParseJavaMojo extends I18nParseMojoSupport {

    public static final String DEFAULT_INCLUDES = "**/*.java";

    /** Root directory of the default entry. */
    @Parameter(property = "i18n.defaultBasedir", defaultValue = "${basedir}/src/main/java")
    private File defaultBasedir;

    /**
     * Default included files to process (ant-like expression).
     * <p>
     * <strong>Note:</strong> default value is **\/*.java
     */
    @Parameter(property = "i18n.defaultIncludes", defaultValue = DEFAULT_INCLUDES, required = true)
    private String defaultIncludes;

    /**
     * Defines the file name of the getter where to put detected i18n keys
     * while getter phase.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.outputGetter", defaultValue = "java.getter")
    private String outputGetter;

    @Override
    public String[] getDefaultIncludes() {
        return new String[]{defaultIncludes};
    }

    @Override
    public String[] getDefaultExcludes() {
        return I18nSourceEntry.EMPTY_STRING_ARRAY;
    }

    @Override
    public File getDefaultBasedir() {
        return defaultBasedir;
    }

    @Override
    public FileUpdater newFileUpdater(SourceEntry entry) {
        return FileUpdaterHelper.newJavaFileUpdater(entry.getBasedir(), getBuildOutputDirectory());
    }

    @Override
    protected String getOutGetter() {
        return outputGetter;
    }

    @Override
    public FileParser newFileParser(File basedir, Pattern acceptPattern) {

        return new JavaFileParser(getLog(), getEncoding(), null, acceptPattern);
    }

    protected static class JavaFileParser extends AbstractFileParser {

        JavaFileParser(Log log, Charset encoding, SortedProperties oldParser, Pattern acceptKeyPattern) {
            super(log, encoding, acceptKeyPattern);
        }

        @Override
        public void parseFile(File file) throws IOException {

            // load content
            String content = new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8);

            // quick check if there is a io.ultreia.java4all.i18n.I18n match in content to avoid a costy parsing
            if (content.contains("io.ultreia.java4all.i18n.I18n")) {

                TokenStream tokenStream = new CommonTokenStream(new Java8Lexer(CharStreams.fromString(content)));
                Java8Parser parser = new Java8Parser(tokenStream);

                try {

                    // see http://stackoverflow.com/a/32918434/2038100
                    //parser.setErrorHandler(new BailErrorStrategy());
                    //parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
                    //parser.getInterpreter().tail_call_preserves_sll = false;
                    parser.getInterpreter().enable_global_context_dfa = true;

                    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();
                    compilationUnitContext.accept(new JavaParserVisitor(file));

                } /*catch (ParseCancellationException e) {
                    // see http://stackoverflow.com/a/32918434/2038100
                    parser.setErrorHandler(new DefaultErrorStrategy());
                    parser.getInterpreter().setPredictionMode(PredictionMode.LL);
                    // parser.getInterpreter().tail_call_preserves_sll = false;
                    // parser.getInterpreter().enable_global_context_dfa = true;

                    Java8Parser.CompilationUnitContext compilationUnitContext = parser.compilationUnit();
                    compilationUnitContext.accept(new JavaParserVisitor(file));

                } */ catch (Exception e) {
                    throw new ParserException(e);
                }

            }
        }

        @Override
        public void parseLine(File file, String line) {
        }

        protected class JavaParserVisitor extends Java8BaseVisitor<Void> {

            protected final File file;
            final Set<String> simpleI18nMethodPrefix;
            final Set<String> complexI18nMethodPrefix;

            private JavaParserVisitor(File file) {
                this.file = file;
                simpleI18nMethodPrefix = new HashSet<>();
                complexI18nMethodPrefix = new HashSet<>();
                simpleI18nMethodPrefix.add("org.nuiton.i18n.I18n.n");
                simpleI18nMethodPrefix.add("org.nuiton.i18n.I18n.t");
                simpleI18nMethodPrefix.add("io.ultreia.java4all.i18n.I18n.n");
                simpleI18nMethodPrefix.add("io.ultreia.java4all.i18n.I18n.t");
                simpleI18nMethodPrefix.add("I18n.n");
                simpleI18nMethodPrefix.add("I18n.t");
                simpleI18nMethodPrefix.add("n");
                simpleI18nMethodPrefix.add("t");
                complexI18nMethodPrefix.add("io.ultreia.java4all.i18n.I18n.l");
                complexI18nMethodPrefix.add("I18n.l");
                complexI18nMethodPrefix.add("l");
            }

            @Override
            public Void visitMethodInvocation_lfno_primary(MethodInvocation_lfno_primaryContext ctx) {

                ArgumentListContext list = ctx.argumentList();
                MethodNameContext mnc = ctx.methodName();
                TerminalNode tn = ctx.Identifier();
                TypeNameContext tnc = ctx.typeName();

                Void aVoid = null;
                if (!visitMethod(list, mnc, tn, tnc)) {
                    // continue visit
                    aVoid = super.visitMethodInvocation_lfno_primary(ctx);
                }
                return aVoid;
            }

            @Override
            public Void visitMethodInvocation(MethodInvocationContext ctx) {
                ArgumentListContext list = ctx.argumentList();
                MethodNameContext mnc = ctx.methodName();
                TerminalNode tn = ctx.Identifier();
                TypeNameContext tnc = ctx.typeName();

                Void aVoid = null;
                if (!visitMethod(list, mnc, tn, tnc)) {
                    // continue visit
                    aVoid = super.visitMethodInvocation(ctx);
                }
                return aVoid;
            }

            boolean visitMethod(ArgumentListContext list, MethodNameContext mnc, TerminalNode tn, TypeNameContext tnc) {

                boolean match = false;

                if (mnc != null || (tnc != null && tn != null)) {
                    String methodName = mnc != null ? mnc.getText() : tnc.getText() + "." + tn.getText();

                    if (simpleI18nMethodPrefix.contains(methodName)) {
                        // key is argument 1 of child 2
                        ExpressionContext argument = list.expression(0);
                        match = register(argument);
                    } else if (complexI18nMethodPrefix.contains(methodName)) {
                        // key is argument 2 of child 2
                        ExpressionContext argument = list.expression(1);
                        match = register(argument);
                    }
                }

                return match;
            }

            private boolean register(ExpressionContext argument) {
                String firstArgs = argument.getText();
                if (firstArgs.matches("^\"[^\"]+\"$")) {
                    String key = firstArgs.substring(1).substring(0, firstArgs.length() - 2);
                    if (getLog().isDebugEnabled()) {
                        getLog().debug(String.format("%s detected key = %s", file.getName(), key));
                    }
                    registerKey(file, key);
                    return true;
                }
                return false;
            }
        }
    }
}
