/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.bundle;

/**
 * Contract to make a conversion from a syntax to another one.
 *
 *
 * Created: 05/05/11
 *
 * @author Florian Desbois
 * @since 2.4
 */
public interface BundleFormatConverter {

    /**
     * Convert format syntax to an other from {@code value}.
     *
     * @param value message with some format placeholders to convert
     * @return the message with other format syntax
     */
    String convert(String value);

}
