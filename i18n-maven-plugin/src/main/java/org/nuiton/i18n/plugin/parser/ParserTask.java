package org.nuiton.i18n.plugin.parser;

/*-
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import io.ultreia.java4all.lang.Strings;
import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.util.List;

/**
 * This is a task to parse a {@link #file}.
 * <p>
 * The task will be executed in the executor service created in the thread.
 */
public class ParserTask implements Runnable, I18nParseMojoConfiguration {

    /**
     * the incoming configuration (from mojo which contains shared result and
     * logger)
     */
    protected final I18nParseMojoConfiguration configuration;

    /** the file parser */
    protected final FileParser parser;

    /** the file to parse */
    protected final File file;

    /** starting time */
    private long startingTime;

    /** ending time */
    private long endingTime;

    ParserTask(I18nParseMojoConfiguration configuration, FileParser parser, File file) {
        this.configuration = configuration;
        this.parser = parser;
        this.file = file;
    }

    @Override
    public boolean isVerbose() {
        return configuration.isVerbose();
    }

    @Override
    public boolean isSilent() {
        return configuration.isSilent();
    }

    @Override
    public boolean isShowTouchedFiles() {
        return configuration.isShowTouchedFiles();
    }

    @Override
    public Log getLog() {
        return configuration.getLog();
    }

    @Override
    public I18nKeySet getGetterFile() {
        return configuration.getGetterFile();
    }

    @Override
    public void run() {
        startingTime = System.nanoTime();
        if (getLog().isDebugEnabled()) {
            getLog().debug(String.format("starting action for %s", file));
        }
        try {

            parser.parseFile(file);

        } catch (Exception e) {
            if (getLog().isErrorEnabled()) {
                getLog().error(String.format("could not parse file %s", file), e);
            }
        } finally {

            if (getLog().isDebugEnabled()) {
                getLog().debug(String.format("ending action for %s", file));
            }
            endingTime = System.nanoTime();
        }
    }

    @Override
    public String toString() {
        return super.toString() + " - " + file;
    }

    protected File getFile() {
        return file;
    }

    private long getDelay() {
        return endingTime - startingTime;
    }

    private void destroy() {
        parser.destroy();
    }

    /**
     * Register the result of the parsing of the {@link #file} after {@link
     * #run()} method was invoked.
     * <p>
     * This method should be invoked by the executor as an ending hook.
     *
     * @param treatedFiles list of files already treated
     * @param touchedFiles list of files already touched
     * @param result       shared result.
     */
    synchronized void registerResult(List<File> treatedFiles, List<File> touchedFiles, I18nKeySet result) {

        try {
            treatedFiles.add(file);
            if (getLog().isDebugEnabled()) {
                String delay = Strings.convertTime(getDelay());
                getLog().debug(String.format("[%d] %s in %s", treatedFiles.size(), file, delay));
            }

            if (parser.isTouched()) {

                // mark file as touched
                touchedFiles.add(file);

                if (isShowTouchedFiles()) {
                    getLog().info("touch " + file);
                }
                if (isVerbose()) {
                    String delay = Strings.convertTime(getDelay());
                    getLog().info(String.format("[%d] file(s) touched %s in %s", treatedFiles.size(), file, delay));
                }

                // merge file result with
                // merge result
                result.addKeys(parser.getResult());
            }
        } finally {

            // destroy runner
            destroy();
        }
    }
}
