/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import io.ultreia.java4all.i18n.spi.builder.I18nKeySet;
import org.apache.maven.plugin.logging.Log;

/**
 * Shared configuration for a parser mojo.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public interface I18nParseMojoConfiguration {

    boolean isVerbose();

    boolean isSilent();

    boolean isShowTouchedFiles();

    Log getLog();

    I18nKeySet getGetterFile();
}
