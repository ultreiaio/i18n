package org.nuiton.i18n.plugin;

/*-
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nCoordinate;
import io.ultreia.java4all.i18n.spi.I18nKeySetDefinition;
import io.ultreia.java4all.i18n.spi.I18nLocaleHelper;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nModuleConfiguration;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

/**
 * Use this goal to set global configuration shared by any other mojo that uses i18n system to detect keys.
 * <p>
 * In your mojo then use {@link I18nModule} with {@code project.getId()} as key.
 * <p>
 * Created by tchemit on 24/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "init", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class I18nInitMojo extends AbstractPlugin {

    /**
     * Group id to use for i18n coordinates.
     *
     * @see I18nCoordinate#getPackageName()
     */
    @Parameter(property = "i18n.groupId", defaultValue = "${project.groupId}", readonly = true)
    private String groupId;
    /**
     * Name to use for i18n coordinates.
     *
     * @see I18nCoordinate#getName()
     */
    @Parameter(property = "i18n.artifactId", defaultValue = "${project.artifactId}", readonly = true)
    private String artifactId;
    /**
     * Locales to treat, separated by comma.
     * <p>
     * Example :
     * <pre>fr_FR,en_GB</pre>
     */
    @Parameter(property = "i18n.bundles", defaultValue = "fr_FR,en_GB", required = true)
    private String bundles;

    /**
     * Encoding used to load and store properties.
     */
    @Parameter(property = "i18n.encoding", defaultValue = "${project.build.sourceEncoding}", required = true)
    private String encoding;

    /**
     * Directory where to find project translations.
     */
    @Parameter(property = "i18n.sourceDirectory", defaultValue = "${basedir}/src/main/i18n/" + I18nTranslationSetDefinition.PATH, required = true)
    private File translationDirectory;
    /**
     * Template extension.
     */
    @Parameter(property = "i18n.templateExtension", defaultValue = I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION, required = true)
    private String templateExtension;
    /**
     * Directory where to find project templates.
     */
    @Parameter(property = "i18n.templateDirectory", defaultValue = "${basedir}/src/main/i18n/" + I18nTemplateDefinition.PATH, required = true)
    private File templateDirectory;
    /**
     * Directory where to generate getters files if not persisted.
     */
    @Parameter(property = "i18n.generatedGettersDirectory", defaultValue = "${project.build.directory}/i18n/" + I18nKeySetDefinition.PATH, required = true)
    private File generatedGettersDirectory;
    /**
     * Directory where to load and store user getters files if persisted.
     */
    @Parameter(property = "i18n.persistedGettersDirectory", defaultValue = "${basedir}/src/main/i18n/" + I18nKeySetDefinition.PATH, required = true)
    private File persistedGettersDirectory;
    /**
     * Directory where to load and store user getters files if persisted.
     */
    @Parameter(property = "i18n.configurationCacheFile", defaultValue = "${basedir}/.mvn/i18n-configuration.properties", required = true)
    private File configurationCacheFile;
    /**
     * To persist getters (in that case the {@code userGettersDirectory} will be used instead of {@code generatedGettersDirectory}.
     * <p>
     * Moreover, the getters won't be deleted in that case ({@code keepGetters} will be always be true.
     */
    @Parameter(property = "i18n.persistGetters", defaultValue = "false")
    private boolean persistGetters;
    /**
     * Strict mode to only keep in user i18n detected i18n keys and remove obsolete keys.
     * <p>
     * <b>Note :</b> By default not active. Use this with care since it can
     * delete keys. Moreover if this flag is activated, then all files will be parsed.
     */
    @Parameter(property = "i18n.strictMode", defaultValue = "false")
    private boolean strictMode;
    /**
     * Verbose flag.
     */
    @Parameter(property = "i18n.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;
    /**
     * Skip flag.
     */
    @Parameter(property = "i18n.skip")
    private boolean skip;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    private Set<Locale> locales;

    @Override
    protected void init() {
        locales = I18nLocaleHelper.parseLocalesAsSet(bundles);
        if (locales.isEmpty()) {
            throw new IllegalStateException("You need at least one locale, please fill the 'bundles' property.");
        }
    }

    @Override
    protected boolean checkSkip() {
        boolean result = super.checkSkip();
        if (result && skip) {
            getLog().info("Skip - by configuration.");
            result = false;
        }
        if (result && !needInvoke(true, false, I18nMojoSupport.getProjectCacheKey(this))) {
            getLog().info("Skip - already executed.");
            result = false;
        }
        return result;
    }

    @Override
    protected void doAction() throws IOException {
        I18nModuleConfiguration i18nModuleConfiguration = new I18nModuleConfiguration(
                groupId,
                artifactId,
                generatedGettersDirectory.toPath(),
                persistedGettersDirectory.toPath(),
                translationDirectory.toPath(),
                templateDirectory.toPath(),
                locales,
                Charset.forName(encoding),
                persistGetters,
                !strictMode,
                templateExtension);
        String serialize = Objects.requireNonNull(i18nModuleConfiguration).serialize();
        I18nModuleConfiguration.store(project.getProperties(), serialize);
        writeFile(configurationCacheFile, serialize, encoding);
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

}
