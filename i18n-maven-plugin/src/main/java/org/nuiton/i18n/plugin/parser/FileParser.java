/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.TreeSet;


/**
 * the contract of a i18n file parser.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public interface FileParser {

    /**
     * Gets encoding used to read and write files.
     *
     * @return the encoding
     */
    Charset getEncoding();

    /**
     * @return {@code true} if file was touched (says contains at least one i18n key)
     */
    boolean isTouched();

    /**
     * @return the results of i18n keys found for the given file
     */
    TreeSet<String> getResult();

    /**
     * Parse sur un fichier
     *
     * @param file le fichier à parser
     * @throws IOException if any pb
     */
    void parseFile(File file) throws IOException;

    /**
     * Parse une partie du fichier
     *
     * @param file le fichier à parser
     * @param line la ligne à parser
     * @throws IOException if any pb
     */
    void parseLine(File file, String line) throws IOException;

    /** clean file parser. */
    void destroy();
}
