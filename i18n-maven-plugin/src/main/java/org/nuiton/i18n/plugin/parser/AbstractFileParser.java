/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.parser;

import org.apache.maven.plugin.logging.Log;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A abstract implementation of a {@link FileParser} with no logic.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.2
 */
public abstract class AbstractFileParser implements FileParser {

    /** Instance logger */
    private final Log log;

    private final TreeSet<String> result;

    private boolean touched;

    private final Charset encoding;

    protected final Pattern acceptKeyPattern;

    protected AbstractFileParser(Log log, Charset encoding, Pattern acceptKeyPattern) {
        this.log = log;
        this.encoding = encoding;
        this.result = new TreeSet<>();
        this.acceptKeyPattern = acceptKeyPattern;
    }

    @Override
    public boolean isTouched() {
        return touched;
    }

    @Override
    public TreeSet<String> getResult() {
        return result;
    }

    public Charset getEncoding() {
        return encoding;
    }

    @Override
    public void destroy() {
        result.clear();
        touched = false;
    }

    public Log getLog() {
        return log;
    }

    /**
     * Method to invoke when a i18n key was detected .
     *
     * @param file incoming file
     * @param key the i18n key to register
     */
    protected void registerKey(File file, String key) {
        if (acceptKeyPattern != null) {
            Matcher matcher = acceptKeyPattern.matcher(key);
            if (!matcher.matches()) {
                return;
            }
        }

        // register result
        getResult().add(key);

        // one key found in file, so file is marked as touched
        touched = true;
    }

    /**
     * To prepare the file (if any thing to be done before scanning it).
     * <p>
     * By default do nothing, use directly the input file.
     *
     * @param file the incoming file
     * @return the real file to process
     * @throws IOException if any IO problem while preparing file
     * @since 2.1
     */
    protected File prepareFile(File file) throws IOException {
        return file;
    }
}
