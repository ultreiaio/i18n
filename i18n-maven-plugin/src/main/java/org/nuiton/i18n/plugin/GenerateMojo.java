/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import io.ultreia.java4all.i18n.spi.I18nResource;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nTranslationSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.i18n.plugin.bundle.BundleValidation;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * Merge new generated i18n bundles with older existing ones.
 *
 * @author Julien Ruchaud - ruchaud@codelutin.com
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate", threadSafe = true, defaultPhase = LifecyclePhase.GENERATE_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateMojo extends I18nMojoWithI18nModuleSupport {

    /**
     * Strict mode to only keep in user i18n detected i18n keys and remove obsolete keys.
     * <p>
     * <b>Note :</b> By default not active. Use this with care since it can
     * delete keys. Moreover if this flag is activated, then all files will be parsed.
     */
    @Parameter(property = "i18n.strictMode", defaultValue = "false")
    private boolean strictMode;

    /**
     * A flag to check that bundles are complete (no missing i18n translations).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.checkBundle", defaultValue = "true", required = true)
    private boolean checkBundle;

    /**
     * A flag to show missing i18n translation.
     * <p>
     * <b>Note :</b> Need the {@link #checkBundle} to be activated).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.showEmpty", defaultValue = "false", required = true)
    private boolean showEmpty;

    /**
     * A flag to make the build fails if there is some missing key values.
     * <p>
     * <b>Note :</b> This parameter should be used in a release profile to ensure bundles are complete.
     *
     * @since 3.5.1
     */
    @Parameter(property = "i18n.failsIfAnyKeyMissingValue", defaultValue = "false")
    private boolean failsIfAnyKeyMissingValue;

    /**
     * A flag to make the build fails if there is some missing keys.
     * <p>
     * <b>Note :</b> This parameter should be used in a release profile to ensure bundles are complete.
     *
     * @since 3.5.1
     */
    @Parameter(property = "i18n.failsIfAnyKeyMissingInBundle", defaultValue = "false")
    private boolean failsIfAnyKeyMissingInBundle;

    /**
     * To remove any keys already existing in the module i18n dependencies and whic values diverge.
     * <p>
     * <b>Note: </b>Be ware, you could loose some overriden translations using this option.
     * <p>
     * <b>Note: </b>By default, the property is not active since we do not want to remove any keys.
     *
     * @since 4.0
     */
    @Parameter(property = "i18n.removeDuplicatedKeys", defaultValue = "false")
    private boolean removeDuplicatedKeys;

    /**
     * To fail build if there is any keys already existing in the module i18n dependencies.
     * <p>
     * <b>Note: </b>By default, the property is active: this is the default behaviour to not override some i18n
     * translations.
     * <p>
     * For projects migrating from version 3.0, your build could breaks with this new options, but for the best I suppose.
     *
     * @since 4.0
     */
    @Parameter(property = "i18n.failsIfDuplicatedKeys", defaultValue = "true")
    private boolean failsIfDuplicatedKeys;

    /**
     * To copy i18n bundle to classes.
     * <b>Note:</b> The default behaviour is to do this copy, in some special cases you may want to override this
     * (for example for a final application using the bundle mojo).
     *
     * @since 4.0
     */
    @Parameter(property = "i18n.copyToClasses", defaultValue = "true")
    private boolean copyToClasses;

    @Override
    protected boolean checkSkip() {
        boolean result = super.checkSkip();
        if (!result) {
            copyI18nBundleToClasses();
        }
        return result;
    }

    @Override
    protected void doAction() throws Exception {
        Set<Locale> locales = getLocales();

        Log log = getLog();

        I18nModule i18nModule = getI18nModule();
        I18nMojoHelper helper = getHelper();
        if (isNotSilent()) {
            log.info("config - sourceDirectory : " + i18nModule.getConfiguration().getTranslationsDirectory());
            log.info("config - locales         : " + locales);
        }

        BundleValidation bundleValidation = new BundleValidation(locales);

        Set<String> moduleKeys = i18nModule.getModuleKeys();
        log.info("Got " + moduleKeys.size() + " key(s) from module.");
        Set<String> detectedKeys = new TreeSet<>(moduleKeys);

        Set<String> dependenciesGetterFilesKeys = i18nModule.getDependenciesKeys();
        log.info("Got " + dependenciesGetterFilesKeys.size() + " key(s) from dependencies.");
        detectedKeys.addAll(dependenciesGetterFilesKeys);

        for (Locale locale : locales) {
            if (isNotSilent()) {
                log.info("prepare bundle for locale " + locale);
            }

            I18nTranslationSet i18nTranslationFile = i18nModule.getModuleTranslation(locale);

            Properties propertiesOut = i18nTranslationFile.getTranslations();

            // Use this one to copy (since strict mode requires propertiesOut to be clear before)
            Properties propertiesOutCopy = new Properties();
            propertiesOutCopy.putAll(propertiesOut);
            propertiesOut.clear();

            Set<String> keysToKeep = new TreeSet<>(detectedKeys);

            if (!strictMode) {
                // Also add source keys
                keysToKeep.addAll(propertiesOutCopy.stringPropertyNames());
            }

            for (String key : keysToKeep) {
                String value = propertiesOutCopy.getProperty(key);
                if (value == null) {
                    // new key
                    value = "";
                }
                propertiesOut.put(key, value);
            }

            log.info(String.format("For locale %s got %d key(s).", locale, propertiesOut.size()));

            List<I18nTranslationSet> dependenciesTranslations = i18nModule.getDependenciesTranslations(locale);

            Set<String> dependenciesKeys = new TreeSet<>();
            Properties dependenciesBundle = new Properties();
            if (!dependenciesTranslations.isEmpty()) {
                for (I18nTranslationSet I18nTranslationSet : dependenciesTranslations) {
                    Properties translations = I18nTranslationSet.getTranslations();
                    dependenciesBundle.putAll(translations);
                    translations.keySet().forEach(e -> dependenciesKeys.add((String) e));
                }
            }

            Set<String> allKeys = new TreeSet<>();
            propertiesOut.keySet().forEach(e -> allKeys.add((String) e));

            allKeys.retainAll(dependenciesKeys);

            Properties moduleDivergence = new Properties();
            if (!allKeys.isEmpty()) {

                Iterator<String> iterator = allKeys.iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    String moduleValue = propertiesOut.getProperty(key);
                    String dependenciesValue = dependenciesBundle.getProperty(key);
                    if (StringUtils.isEmpty(moduleValue) || Objects.equals(moduleValue, dependenciesValue)) {
                        if (isVerbose()) {
                            log.info(String.format("For locale %s, removing duplicated key %s", locale, key));
                        }
                        iterator.remove();
                        propertiesOut.remove(key);
                    } else {
                        moduleDivergence.put(key, Pair.of(dependenciesValue, moduleValue));
                    }
                }
            }
            if (!allKeys.isEmpty()) {

                // Got overrides keys in this module

                if (failsIfDuplicatedKeys) {

                    throw new MojoFailureException(
                            String.format("For locale %s found %d duplicated key(s):\n%s\n\nDivergence values:\n%s", locale, allKeys.size(), Joiner.on("\n").join(allKeys), Joiner.on("\n").join(moduleDivergence.entrySet())));
                }

                log.warn(String.format("For locale %s found %d duplicated key(s):\n%s\n\nDivergence values:\n%s", locale, allKeys.size(), Joiner.on("\n").join(allKeys), Joiner.on("\n").join(moduleDivergence.entrySet())));

                if (removeDuplicatedKeys) {

                    allKeys.forEach(propertiesOut::remove);
                    log.info(String.format("For locale %s, remove %d duplicated key(s).", locale, allKeys.size()));
                }
            }

            if (isNotSilent()) {
                log.info(String.format("merge bundle %s to outputDirectory", locale));
            }

            if (checkBundle) {

                ImmutableSet<String> keys = Maps.fromProperties(propertiesOut).keySet();
                bundleValidation.getKeysPerLocale().putAll(locale, keys);


                helper.checkBundle(locale, propertiesOut, showEmpty, bundleValidation);
            }

            i18nModule.storeModuleTranslation(i18nTranslationFile);

            if (isNotSilent()) {
                log.info(String.format("copy bundle %s to sourceDirectory", locale));
            }
        }

        if (!i18nModule.getConfiguration().isPersistGetters()) {
            i18nModule.deleteModuleGetters();
        }

        helper.failsIfAnyKeyMissingValue(failsIfAnyKeyMissingValue, bundleValidation);
        helper.failsIfAnyKeyMissingInBundle(failsIfAnyKeyMissingInBundle, bundleValidation);

        copyI18nBundleToClasses();

    }

    private void copyI18nBundleToClasses() {
        if (!copyToClasses) {
            return;
        }
        Path translationsDirectory = getBuildOutputDirectory().toPath().resolve(I18nTranslationSetDefinition.I18N_CLASS_PATH);
        getLog().info("Copy i18n translations to: " + translationsDirectory);
        try {
            getI18nModule().exportModuleTranslations(translationsDirectory);
        } catch (IOException e) {
            throw new RuntimeException("Can't copy i18n module translations", e);
        }
    }
}
