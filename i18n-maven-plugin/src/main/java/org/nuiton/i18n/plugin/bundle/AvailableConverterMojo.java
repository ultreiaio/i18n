/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.bundle;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.plugin.AbstractAvailableDataMojo;

/**
 * Mojo used to display available {@link BundleFormatConverter}.
 * <p>
 * Created: 09/05/11
 *
 * @author Florian Desbois
 * @since 2.4
 */
@Mojo(name = "available-converters", requiresProject = true, requiresDirectInvocation = true, requiresDependencyResolution = ResolutionScope.TEST)
public class AvailableConverterMojo extends AbstractAvailableDataMojo {

    /**
     * Map of all availables {@link BundleFormatConverter}.
     *
     * @since 2.4
     */
    @Component(role = BundleFormatConverter.class)
    private Map<String, BundleFormatConverter> bundleFormatConverters;

    @Override
    protected Collection<AvailableData> getAllAvailableDatas() {

        AvailableData data = new AvailableData() {

            @Override
            public String name() {
                return "bundleFormatConverter";
            }

            @Override
            public Map<String, ?> getData() {
                return bundleFormatConverters;
            }

            @Override
            public String toString(Object value) {
                return value.getClass().getName();
            }
        };

        return Collections.singletonList(data);
    }

}
