/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.i18n.plugin.bundle;

import java.text.MessageFormat;
import java.util.Formatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.codehaus.plexus.component.annotations.Component;

/**
 * Implementation of {@link BundleFormatConverter} to convert {@link Formatter}
 * syntax to a {@link MessageFormat} syntax.
 * <p>
 * Created: 05/05/11
 *
 * @author Florian Desbois
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.4
 */
@Component(role=BundleFormatConverter.class, hint = "toMessageFormat")
public class StringFormatToMessageFormatConverter implements BundleFormatConverter {

    /** Logger. */
    private static final Logger log =
            LogManager.getLogger(StringFormatToMessageFormatConverter.class);

    private static Pattern PATTERN = Pattern.compile("%\\$?(\\d?)[^\\s']*");

    @Override
    public String convert(String value) {

        String result;

        Matcher matcher = PATTERN.matcher(value);
        boolean matches = matcher.find();

        if (log.isDebugEnabled()) {
            log.debug(String.format("> value : %s _ matches ? %s", value, matches));
        }

        if (matches) {

            // Reset done, because of first find()
            matcher.reset();

            StringBuffer sb = new StringBuffer();
            int i = 0;

            while (matcher.find()) {

                if (log.isDebugEnabled()) {
                    log.debug(String.format("> match group : %s", matcher.group(0)));
                    log.debug(String.format("> match group for number : %s", matcher.group(1)));
                }

                String argNumber = matcher.group(1);

                int nb;

                if (StringUtils.isNotEmpty(argNumber)) {

                    // there is a arg position number, so must use the -1 value
                    nb = Integer.parseInt(argNumber) - 1;
                } else {

                    // use the current argument position value
                    nb = i;
                }

                // Append replacement for current occurence
                matcher.appendReplacement(sb, "\\{" + nb + "\\}");
                i++;
            }
            // Append last chars from input String
            matcher.appendTail(sb);
            result = sb.toString();

            if (log.isDebugEnabled()) {
                log.debug(String.format("Result : %s", result));
            }

        } else {

            // there is no argument in incoming string value
            result = value;
        }

        // Always escape quote ' to ''
        result = escapeQuoteChar(result);

        return result;
    }

    /**
     * Escape ' char with '', needed by {@link MessageFormat}.
     *
     * @param value Message that contains '
     * @return the message with ' escaped
     */
    private String escapeQuoteChar(String value) {

        // Replace ' by ''
        String result = value.replaceAll("'", "''");

        if (log.isDebugEnabled()) {
            log.debug(String.format("Result with ' escape : %s", result));
        }

        return result;
    }

}
