package org.nuiton.i18n.plugin;

/*-
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nKeySetDefinition;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nModuleConfiguration;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.IOException;
import java.net.URLClassLoader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.Locale;
import java.util.Set;

/**
 * Created by tchemit on 30/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class I18nMojoWithI18nModuleSupport extends I18nMojoSupport {
    /**
     * To force multiple execution on a same maven module.
     */
    @Parameter(property = "i18n.force")
    private boolean force;

    private I18nModule i18nModule;

    @Override
    public void init() throws Exception {
        super.init();
        URLClassLoader classLoader = initClassLoader(getProject(), null, false, false, false, true, true);
        i18nModule = I18nModule.builder(getProject().getProperties())
                .loadDependencies(classLoader)
                .loadKeySets(true)
                .loadTranslations()
                .loadTemplates()
                .build();
    }

    @Override
    protected boolean checkSkip() {
        boolean result = super.checkSkip();
        if (result && i18nModule.isEmpty()) {
            getLog().info("Skip - No getters, nor translations, nor templates  detected in module and dependencies.");
            result = false;
        }
        if (result && !force && !needInvoke(true, false, I18nMojoSupport.getProjectCacheKey(this))) {
            getLog().info("Skip - already executed.");
            result = false;
        }
        return result;
    }

    protected I18nModule getI18nModule() {
        return i18nModule;
    }

    protected Charset getEncoding() {
        return getI18nModuleConfiguration().getEncoding();
    }

    protected I18nModuleConfiguration getI18nModuleConfiguration() {
        return i18nModule.getConfiguration();
    }

    protected Set<Locale> getLocales() {
        return getI18nModuleConfiguration().getLocales();
    }

    protected void copyModuleResources(I18nModule i18nModule, Path buildOutputDirectory) throws IOException {

        if (i18nModule.withModuleGetters() && i18nModule.getConfiguration().isPersistGetters()) {

            Path gettersDirectory = buildOutputDirectory.resolve(I18nKeySetDefinition.I18N_CLASS_PATH);

            Path path = i18nModule.exportModuleGetters(gettersDirectory);
            getLog().info("Copy module getters to: " + path);
        }

        if (i18nModule.withModuleTemplates()) {
            Path templatesDirectory = buildOutputDirectory.resolve(I18nTemplateDefinition.I18N_CLASS_PATH);
            i18nModule.exportModuleTemplates(templatesDirectory);
            getLog().info("Copy module templates to: " + templatesDirectory);
        }
    }
}
