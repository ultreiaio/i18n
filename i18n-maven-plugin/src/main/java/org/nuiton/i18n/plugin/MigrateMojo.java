package org.nuiton.i18n.plugin;

/*-
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 26/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "migrate", threadSafe = true, defaultPhase = LifecyclePhase.INITIALIZE)
public class MigrateMojo extends AbstractPlugin {

    /**
     * Directory where old i18n translations files are stored.
     */
    @Parameter(property = "i18n.oldSourceDirectory", defaultValue = "${basedir}/src/main/resources/i18n", required = true)
    private File oldSourceDirectory;
    /**
     * Directory where to find project translations.
     */
    @Parameter(property = "i18n.sourceDirectory", defaultValue = "${basedir}/src/main/i18n/translations", required = true)
    private File sourceDirectory;
    /**
     * Verbose flag.
     * <p>
     * <b>Note :</b> if not set, we used the {@code maven.verbose} property.
     */
    @Parameter(property = "i18n.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;

    @Parameter(property = "i18n.force", defaultValue = "false")
    private boolean force;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    protected void init() {
    }

    @Override
    protected boolean checkSkip() {
        boolean canContinue = super.checkSkip();
        if (canContinue && !oldSourceDirectory.exists()) {
            canContinue = false;
            getLog().info("Skip - no i18n old directory " + oldSourceDirectory + " found.");
        }
        return canContinue;
    }

    @Override
    protected void doAction() throws Exception {
        Path oldPath = oldSourceDirectory.toPath();
        Path newPath = sourceDirectory.toPath();
        if (!Files.exists(newPath)) {
            Files.createDirectories(newPath);
        }
        for (Path oldFilePath : Files
                .walk(oldPath, 1)
                .filter(p -> Files.isRegularFile(p) && p.toFile().getName().endsWith(".properties"))
                .collect(Collectors.toList())) {
            Path newFilePath = newPath.resolve(oldFilePath.toFile().getName());
            if (verbose) {
                getLog().info(String.format("Move %s to %s", oldFilePath, newFilePath));
            }
            if (Files.exists(newPath)) {
                if (force) {
                    Files.move(oldFilePath, newFilePath, StandardCopyOption.REPLACE_EXISTING);
                } else {
                    Files.deleteIfExists(newPath);
                }
            } else {
                Files.move(oldFilePath, newFilePath);
            }
        }
        if (verbose) {
            getLog().info(String.format("Delete old directory %s", oldPath));
        }
        Files.delete(oldPath);

    }
}
