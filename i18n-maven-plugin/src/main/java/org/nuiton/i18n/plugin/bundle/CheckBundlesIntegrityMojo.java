/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin.bundle;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.SortedSetMultimap;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nTranslationSet;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.i18n.plugin.I18nMojoHelper;
import org.nuiton.i18n.plugin.I18nMojoWithI18nModuleSupport;

import java.util.Collection;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Check bundles integrity. That all keys have a value in all bundles.
 *
 * @since 3.5
 */
@Mojo(name = "check-bundles-integrity", threadSafe = true, defaultPhase = LifecyclePhase.PREPARE_PACKAGE, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class CheckBundlesIntegrityMojo extends I18nMojoWithI18nModuleSupport {

    /**
     * A flag to show empty i18n translation.
     * <p>
     * <b>Note :</b> Need the {@link I18nMojoHelper#checkBundle} to be activated).
     *
     * @since 1.0.0
     */
    @Parameter(property = "i18n.showEmpty", defaultValue = "false", required = true)
    private boolean showEmpty;

    /**
     * A flag to show missing i18n translation.
     *
     * @since 3.5
     */
    @Parameter(property = "i18n.showMissing", defaultValue = "false", required = true)
    private boolean showMissing;

    /**
     * A flag to make the build fails if there is some warnings while generating
     * bundle, says when it misses some translations.
     * <p>
     * <b>Note :</b> This parameter should be used in a release profile to
     * ensure bundles are complete.
     *
     * @since 2.0
     */
    @Parameter(property = "i18n.failsIfWarning", defaultValue = "false")
    private boolean failsIfWarning;

    @Override
    protected void doAction() throws Exception {
        Set<Locale> locales = getLocales();

        I18nModule i18nModule = getI18nModule();

        if (isNotSilent()) {
            getLog().info("config - locales         : " + locales);
        }

        I18nMojoHelper helper = getHelper();
        BundleValidation bundleValidation = new BundleValidation(locales);

        for (Locale locale : locales) {

            I18nTranslationSet moduleTranslation = i18nModule.getModuleTranslation(locale);
            if (moduleTranslation != null) {

                Properties translations = moduleTranslation.getTranslations();
                helper.checkBundle(locale, translations, showEmpty, bundleValidation);

                ImmutableSet<String> keys = Maps.fromProperties(translations).keySet();
                bundleValidation.getKeysPerLocale().putAll(locale, keys);
            }
        }
        String artifactId = i18nModule.getName();
        SortedSetMultimap<Locale, String> missingKeysPerLocale = bundleValidation.getMissingKeysPerLocale();
        if (missingKeysPerLocale.isEmpty()) {
            if (isNotSilent()) {
                getLog().info(String.format("all locales for bundle %s are consistent", artifactId));
            }
        } else {
            if (showMissing) {
                for (Map.Entry<Locale, String> entry : missingKeysPerLocale.entries()) {
                    Locale locale = entry.getKey();
                    String missingKey = entry.getValue();
                    getLog().warn(String.format("bundle %s for locale %s misses key %s", artifactId, locale, missingKey));
                }
            } else {
                for (Map.Entry<Locale, Collection<String>> entry : missingKeysPerLocale.asMap().entrySet()) {
                    Locale locale = entry.getKey();
                    int numberOfMissingKeys = entry.getValue().size();
                    getLog().warn(String.format("bundle %s for locale %s misses %d keys! (use -Di18n.showMissing to see these entries)", artifactId, locale, numberOfMissingKeys));
                }
            }
        }

        if (failsIfWarning && bundleValidation.isFail()) {
            // there is at least one not complete bundle, fail the build
            throw new MojoFailureException("Bundles validation failed, see warning above for details about how to fix");
        }
    }
}
