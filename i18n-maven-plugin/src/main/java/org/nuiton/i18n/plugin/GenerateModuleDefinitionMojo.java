package org.nuiton.i18n.plugin;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 22/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-module-definition", threadSafe = true, defaultPhase = LifecyclePhase.PROCESS_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class GenerateModuleDefinitionMojo extends I18nMojoWithI18nModuleSupport {

    @Override
    protected void doAction() throws IOException {

        I18nModule i18nModule = getI18nModule();
        copyModuleResources(i18nModule, getBuildOutputDirectory().toPath());

        String moduleName = i18nModule.getName();

        Set<String> moduleGetters = getHelper().keySetToStr(i18nModule.getModuleGetters(), "Load %d module getters%s");
        Set<String> dependenciesGetters = getHelper().keySetToStr(i18nModule.getDependenciesGetters(), "Load %d dependencies getters%s");
        Set<String> dependenciesTranslations = getHelper().translationsToStr(i18nModule.getDependenciesTranslations(), "Load %d dependencies translations%s");
        Set<String> moduleTranslations = getHelper().translationsToStr(i18nModule.getModuleTranslations(), "Load %d module translations%s");
        Set<String> dependenciesTemplates = getHelper().templatesToStr(i18nModule.getDependenciesTemplates(), "Load %d dependencies templates%s");
        Set<String> moduleTemplates = getHelper().templatesToStr(i18nModule.getModuleTemplates(), "Load %d module templates%s");

        String filename = getFilename(i18nModule.getName(), "I18nModuleDefinition");
        Path target = generateJavaFile(i18nModule, "I18nModuleDefinition", writer -> {

            String groupId = i18nModule.getPackageName();

            String moduleKeySetStr = moduleGetters.isEmpty() ? "" : ("\"" + i18nModule.getId() + "\"");
            String dependenciesKeySetStr = dependenciesGetters.isEmpty() ? "" : dependenciesGetters.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",\n                           "));
            String moduleTranslationStr = moduleTranslations.isEmpty() ? "" : moduleTranslations.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",\n                           "));
            String dependenciesTranslationStr = dependenciesTranslations.isEmpty() ? "" : dependenciesTranslations.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",\n                           "));
            String moduleTemplateStr = moduleTemplates.isEmpty() ? "" : moduleTemplates.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",\n                           "));
            String dependenciesTemplateStr = dependenciesTemplates.isEmpty() ? "" : dependenciesTemplates.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",\n                           "));
            String content = String.format(
                    "package %1$s;\n\n" +
                            "import com.google.auto.service.AutoService;\n" +
                            "import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;\n\n" +
                            "import javax.annotation.Generated;\n\n" +
                            "@AutoService(I18nModuleDefinition.class)\n" +
                            "@Generated(value = \"%5$s\", date = \"%6$s\")\n" +
                            "public class %2$s extends I18nModuleDefinition {\n\n" +
                            "    public %2$s() {\n" +
                            "        super(\"%1$s\", \"%3$s\", \"%4$s\",\n" +
                            "              new String[]{%7$s}, // Module getters\n" +
                            "              new String[]{%8$s}, // Dependencies getters\n" +
                            "              new String[]{%9$s}, // Module translations\n" +
                            "              new String[]{%10$s},  // Dependencies translations\n" +
                            "              new String[]{%11$s}, // Module templates\n" +
                            "              new String[]{%12$s}  // Dependencies templates\n" +
                            "        );\n" +
                            "    }\n" +
                            "}\n",
                    groupId,
                    filename,
                    moduleName,
                    i18nModule.getConfiguration().getTemplateExtension(),
                    getClass().getName(),
                    new Date(),
                    moduleKeySetStr,
                    dependenciesKeySetStr,
                    moduleTranslationStr,
                    dependenciesTranslationStr,
                    moduleTemplateStr,
                    dependenciesTemplateStr);
            try {
                writer.write(content);
            } catch (IOException e) {
                throw new RuntimeException("Can't generate java file content", e);
            }
        });

        getLog().info(String.format("Module definition %s generated to %s", filename, target));
    }
}
