package org.nuiton.i18n.plugin.bundle;

/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Ordering;
import com.google.common.collect.Sets;
import com.google.common.collect.SortedSetMultimap;
import com.google.common.collect.TreeMultimap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * The validation result of a validation check over multiple bundles.
 *
 * @since 3.5
 */
public class BundleValidation {

    protected ImmutableSortedSet<Locale> locales;

    /** to keep all none translated i18n keys by locale. */
    private Map<Locale, SortedSet<String>> keysMissingValues = new HashMap<>();

    /**
     * Store all keys declared for each {@link Locale}.
     */
    private SortedSetMultimap<Locale, String> keysPerLocale;

    /**
     * Sort locales according to the order used in the POM file.
     */
    private Ordering<Locale> localeOrdering;

    public BundleValidation(Collection<Locale> locales) {
        localeOrdering = Ordering.explicit(new ArrayList<>(locales));
        this.locales = ImmutableSortedSet.orderedBy(localeOrdering).addAll(locales).build();
        this.keysPerLocale = newEmptyMapForBundles();
    }

    public BundleValidation(Locale[] locales) {
        List<Locale> localesList = Arrays.asList(locales);
        localeOrdering = Ordering.explicit(localesList);
        this.locales = ImmutableSortedSet.orderedBy(localeOrdering).addAll(localesList).build();
        this.keysPerLocale = newEmptyMapForBundles();
    }

    public Map<Locale, SortedSet<String>> getKeysMissingValues() {
        return keysMissingValues;
    }

    public SortedSetMultimap<Locale, String> getKeysPerLocale() {
        return keysPerLocale;
    }

    public SortedSetMultimap<Locale, String> getMissingKeysPerLocale() {
        ImmutableSortedSet<String> allKeysFromAllBundles = ImmutableSortedSet.copyOf(keysPerLocale.values());
        SortedSetMultimap<Locale, String> missingKeysPerLocale = newEmptyMapForBundles();
        for (Locale locale : locales) {
            Set<String> keysInBundle = keysPerLocale.get(locale);
            Set<String> missingKeysInBundle = Sets.symmetricDifference(allKeysFromAllBundles, keysInBundle);
            missingKeysPerLocale.putAll(locale, missingKeysInBundle);
        }
        return missingKeysPerLocale;
    }

    public boolean isAnyKeyMissingInBundle() {
        return ! getMissingKeysPerLocale().isEmpty();
    }

    public boolean isAnyKeyMissingValue() {
        return ! getKeysMissingValues().isEmpty();
    }

    public boolean isFail() {
        return isAnyKeyMissingValue() || isAnyKeyMissingInBundle();
    }

    /**
     * Factory method for a method suitable to deterministically store bundles.
     */
    private SortedSetMultimap<Locale, String> newEmptyMapForBundles() {
        return TreeMultimap.create(localeOrdering, Ordering.natural());
    }
}
