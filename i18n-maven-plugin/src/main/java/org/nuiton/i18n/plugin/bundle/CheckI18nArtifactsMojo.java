package org.nuiton.i18n.plugin.bundle;

/*-
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import io.ultreia.java4all.i18n.spi.builder.I18nTranslationSet;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.nuiton.i18n.plugin.I18nMojoWithI18nModuleSupport;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * Performs some checks on i18n artifacts (remember a i18n artifact is just a maven module using i18n API (it can be
 * your module or any dependency)).
 * <ul>
 * <li>Check <b>convergence</b> : means if a key is present in more than one bundle, then it must have exactly the same translation</li>
 * <li>Check <b>integrity</b> : means that a key should be present for all locales</li>
 * </ul>
 * Created by tchemit on 12/07/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0
 */
@Mojo(name = "check-i18n-artifacts", threadSafe = true, defaultPhase = LifecyclePhase.PROCESS_RESOURCES, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class CheckI18nArtifactsMojo extends I18nMojoWithI18nModuleSupport {

    /**
     * To check if i18n artifacts converge.
     */
    @Parameter(property = "i18n.checkConvergence", defaultValue = "true")
    private boolean checkConvergence;

    /**
     * To check i18n artifacts integrity.
     */
    @Parameter(property = "i18n.checkIntegrity", defaultValue = "true")
    private boolean checkIntegrity;

    @Override
    protected void doAction() throws Exception {

        I18nModule i18nModule = getI18nModule();

        Multimap<Locale, String> allKeysByLocale = HashMultimap.create();
        Map<Locale, Integer> bundlesCount = new LinkedHashMap<>();

        for (Locale locale : getLocales()) {

            Map<String, I18nKey> keys = new TreeMap<>();

            I18nTranslationSet moduleTranslation = i18nModule.getModuleTranslation(locale);
            List<I18nTranslationSet> dependenciesTranslations = i18nModule.getDependenciesTranslations(locale);
            for (I18nTranslationSet I18nTranslationSet : dependenciesTranslations) {
                loadTranslations(allKeysByLocale, locale, keys, I18nTranslationSet);
            }
            if (moduleTranslation != null) {
                loadTranslations(allKeysByLocale, locale, keys, moduleTranslation);
            }
            int translationsCount = dependenciesTranslations.size() + (moduleTranslation == null ? 0 : 1);
            bundlesCount.put(locale, translationsCount);

            if (checkConvergence) {
                getLog().info(String.format("Checking convergence for locale %s", locale));
            }
            if (checkConvergence && keys.values().stream().anyMatch(I18nKey::isNotValid)) {

                StringBuilder builder = new StringBuilder();
                // have some bad keys
                Set<I18nKey> badI18nKeys = keys.values().stream().filter(I18nKey::isNotValid).collect(Collectors.toSet());
                for (I18nKey i18nKey : badI18nKeys) {
                    builder.append("\n").append(i18nKey.getKey());
                    for (Map.Entry<String, String> entry : i18nKey.getTranslations().entrySet()) {
                        String bundle = entry.getKey();
                        String translation = entry.getValue();
                        builder.append(String.format("\n\tbundle %s:\n\t\t%s", bundle, translation));
                    }
                }

                throw new MojoExecutionException(String.format("For locale %s, there is %d divergent i18n key(s):%s", locale, badI18nKeys.size(), builder.toString()));
            } else {
                getLog().info(String.format("%d bundle(s) converge - %d translation(s)", translationsCount, keys.size()));
            }

        }

        if (checkIntegrity) {

            Set<String> allKeys = new HashSet<>(allKeysByLocale.values());

            for (Map.Entry<Locale, Collection<String>> entry : allKeysByLocale.asMap().entrySet()) {

                Locale locale = entry.getKey();
                getLog().info(String.format("Checking integrity for locale %s", locale));

                Set<String> keys = new TreeSet<>(allKeys);
                keys.removeAll(entry.getValue());

                if (!keys.isEmpty()) {

                    throw new MojoExecutionException(String.format("For locale %s, there is missing %d key(s):\n%s", locale, keys.size(), Joiner.on("\n").join(keys)));
                } else {
                    getLog().info(String.format("%d bundle(s) are upstanding - %d translation(s)", bundlesCount.get(locale), allKeys.size()));
                }
            }
        }
    }

    private void loadTranslations(Multimap<Locale, String> allKeysByLocale, Locale locale, Map<String, I18nKey> keys, I18nTranslationSet I18nTranslationSet) {
        for (Map.Entry<Object, Object> entry : I18nTranslationSet.getTranslations().entrySet()) {
            String key = (String) entry.getKey();
            keys.putIfAbsent(key, new I18nKey(locale, key));
            I18nKey i18nKey = keys.get(key);
            i18nKey.addTranslation(I18nTranslationSet.getDefinition().getId(), (String) entry.getValue());
            allKeysByLocale.put(locale, key);
        }
    }

    /**
     * Created by tchemit on 12/07/17.
     *
     * @author Tony Chemit - dev@tchemit.fr
     * @since 4.0
     */
    public static class I18nKey {

        private final Locale locale;
        private final String key;

        private final Map<String, String> translations;

        I18nKey(Locale locale, String key) {
            this.locale = locale;
            this.key = key;
            this.translations = new TreeMap<>();
        }

        void addTranslation(String bundle, String translation) {
            translations.put(bundle, translation);
        }

        public Locale getLocale() {
            return locale;
        }

        public String getKey() {
            return key;
        }

        /**
         * @return {@code true} if i18n key is valid, means all translations are the same, {@code false} otherwise.
         */
        boolean isValid() {
            return new HashSet<>(translations.values()).size() == 1;
        }

        boolean isNotValid() {
            return !isValid();
        }

        public Map<String, String> getTranslations() {
            return translations;
        }
    }
}
