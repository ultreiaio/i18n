/*
 * #%L
 * I18n :: Maven Plugin
 * %%
 * Copyright (C) 2007 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.i18n.plugin;

import io.ultreia.java4all.i18n.spi.builder.I18nModule;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.nuiton.plugin.AbstractPlugin;
import org.nuiton.plugin.PluginHelper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Consumer;

/**
 * Lower level i18n mojo abstraction.
 * <p>
 * We defines here all commons parameters and shared behaviour.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @author Julien Ruchaud - ruchaud@codelutin.com
 */
public abstract class I18nMojoSupport extends AbstractPlugin {

    /**
     * Build directory.
     */
    @Parameter(property = "i18n.buildOutputDirectory", defaultValue = "${project.build.outputDirectory}")
    private File buildOutputDirectory;
    /**
     * The root directory where to generate java code.
     */
    @Parameter(property = "i18n.javaOutputDirectory", defaultValue = "${basedir}/target/generated-sources/i18n", required = true)
    private File javaOutputDirectory;
    /**
     * Verbose flag.
     * <p>
     * <b>Note :</b> if not set, we used the {@code maven.verbose} property.
     */
    @Parameter(property = "i18n.verbose", defaultValue = "${maven.verbose}")
    private boolean verbose;
    /**
     * Silent flag to see only errors in console.
     *
     * @since 1.0.0-rc-5
     */
    @Parameter(property = "i18n.silent", defaultValue = "false")
    private boolean silent;
    /**
     * Skip flag.
     */
    @Parameter(property = "i18n.skip")
    private boolean skip;

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    private I18nMojoHelper helper;

    public static String getProjectCacheKey(AbstractPlugin mojo) {
        MavenProject project = mojo.getProject();
        return mojo.getClass().getSimpleName() + ":" + project.getGroupId() + ":" + project.getArtifactId() + ":" + project.getArtifact().getClassifier() + ":" + project.getVersion();
    }

    @Override
    protected boolean checkSkip() {
        boolean result = super.checkSkip();
        if (skip) {
            getLog().info("Skip - by configuration.");
            result = false;
        }
        return result;
    }

    @Override
    protected boolean checkPackaging() {
        // nothing to do on a pom module
        return !acceptPackaging(Packaging.pom);
    }

    @Override
    public void init() throws Exception {
        helper = new I18nMojoHelper(getLog(), silent, verbose);
        if (verbose) {
            // in verbose mode, no silent
            silent = false;
            getLog().info("config - verbose mode is on");
        }
    }

    @Override
    public File getBackupFile(File file) {
        return new File(file.getAbsolutePath() + "~");
    }

    @Override
    protected void backupFile(File f) throws IOException {
        File dst = getBackupFile(f);
        copyFile(f, dst);
    }

    @Override
    public MavenProject getProject() {
        return project;
    }

    @Override
    public void setProject(MavenProject project) {
        this.project = project;
    }

    @Override
    public boolean isVerbose() {
        return verbose;
    }

    @Override
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public boolean isSilent() {
        return silent;
    }

    public boolean isNotSilent() {
        return !isSilent();
    }

    public File getBuildOutputDirectory() {
        return buildOutputDirectory;
    }

    public I18nMojoHelper getHelper() {
        return helper;
    }

    protected Path generateJavaFile(I18nModule module, String fileNameSuffix, Consumer<BufferedWriter> writerConsumer) throws IOException {

        String packageName = module.getPackageName();
        Path directory = PluginHelper.getFile(javaOutputDirectory, packageName.trim().split("\\.")).toPath();
        String filename = getFilename(module.getName(), fileNameSuffix);
        Files.createDirectories(directory);
        Path file = directory.resolve(filename + ".java");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(file, StandardCharsets.UTF_8)) {
            writerConsumer.accept(bufferedWriter);
        }
        addCompileSourceRoots(javaOutputDirectory);
        return file;
    }


    protected String getFilename(String name, String suffix) {
        StringBuilder filename = new StringBuilder();
        for (String s : name.split("-")) {
            filename.append(StringUtils.capitalize(s));
        }
        filename.append(suffix);
        return filename.toString();
    }

}
