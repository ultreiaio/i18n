package io.ultreia.java4all.i18n.spi.builder;

/*-
 * #%L
 * I18n :: Spi Builder
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nKeySetDefinition;
import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.io.I18nKeySetClassPathReader;
import io.ultreia.java4all.i18n.spi.io.I18nKeySetDirectoryReader;
import io.ultreia.java4all.i18n.spi.io.I18nKeySetReader;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by tchemit on 29/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nKeySet {

    private final I18nKeySetDefinition definition;
    private Set<String> keySet;

    public I18nKeySet(I18nKeySetDefinition definition, Set<String> keySet) {
        this.definition = Objects.requireNonNull(definition);
        this.keySet = Collections.synchronizedSet(new TreeSet<>(Objects.requireNonNull(keySet)));
    }

    static List<I18nKeySet> detectKeySets(ClassLoader classLoader, List<I18nModuleDefinition> i18nModuleDefinitions) throws I18nResourceInitializationException {
        I18nKeySetClassPathReader reader = new I18nKeySetClassPathReader(classLoader, true);
        List<I18nKeySetDefinition> i18nModuleKeySetDefinitions = new LinkedList<>();
        for (I18nModuleDefinition i18nModuleDefinition : i18nModuleDefinitions) {
            i18nModuleKeySetDefinitions.addAll(i18nModuleDefinition.getModuleKeySetDefinitions());
        }
        return read(i18nModuleKeySetDefinitions, reader, true);
    }

    static List<I18nKeySet> detectKeySets(Path directory, String packageName, boolean load) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        List<I18nKeySetDefinition> i18nModuleKeySetDefinitions = I18nKeySetDefinition.detect(directory, packageName);
        I18nKeySetDirectoryReader reader = new I18nKeySetDirectoryReader(directory, false);
        return read(i18nModuleKeySetDefinitions, reader, load);
    }

    private static List<I18nKeySet> read(List<I18nKeySetDefinition> definitions, I18nKeySetReader loader, boolean load) throws I18nResourceInitializationException {
        List<I18nKeySet> result = new LinkedList<>();
        for (I18nKeySetDefinition definition : definitions) {
            Set<String> keySet = load ? loader.read(definition) : Collections.emptySet();
            result.add(new I18nKeySet(definition, keySet));
        }
        return result;
    }

    public I18nKeySetDefinition getDefinition() {
        return definition;
    }

    public Set<String> getKeySet() {
        return keySet;
    }

    public int size() {
        return keySet == null ? 0 : keySet.size();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean isNotEmpty() {
        return !isEmpty();
    }

    public void addKeys(TreeSet<String> newKeys) {
        keySet.addAll(newKeys);
    }

    public void addKey(String newKey) {
        keySet.add(newKey);
    }

    public void store(Path file) {
        try {
            if (!Files.exists(Objects.requireNonNull(file).getParent())) {
                Files.createDirectories(file.getParent());
            }
            Files.write(file, getKeySet(), StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new RuntimeException("Could not store getter file " + file, e);
        }
    }

}
