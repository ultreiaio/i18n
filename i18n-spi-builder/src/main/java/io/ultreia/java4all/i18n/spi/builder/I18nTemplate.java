package io.ultreia.java4all.i18n.spi.builder;

/*-
 * #%L
 * I18n :: Spi Builder
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;
import io.ultreia.java4all.i18n.spi.io.I18nTemplateClassPathReader;
import io.ultreia.java4all.i18n.spi.io.I18nTemplateDirectoryReader;
import io.ultreia.java4all.i18n.spi.io.I18nTemplateReader;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by tchemit on 29/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTemplate {

    private final I18nTemplateDefinition definition;
    private final String template;

    private I18nTemplate(I18nTemplateDefinition definition, String template) {
        this.definition = Objects.requireNonNull(definition);
        this.template = Objects.requireNonNull(template);
    }

    static List<I18nTemplate> detect(ClassLoader classLoader, Charset encoding, List<I18nModuleDefinition> i18nModuleDefinitions) throws I18nResourceInitializationException {
        List<I18nTemplateDefinition> definitions = new LinkedList<>();
        for (I18nModuleDefinition i18nModuleDefinition : i18nModuleDefinitions) {
            definitions.addAll(i18nModuleDefinition.getModuleTemplateSetDefinitions());
        }
        I18nTemplateClassPathReader reader = new I18nTemplateClassPathReader(classLoader, true);
        return read(definitions, encoding, reader);
    }

    static List<I18nTemplate> detect(Path directory, String packageName, String templateExtension, Charset encoding) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        List<I18nTemplateDefinition> definitions = I18nTemplateDefinition.detect(directory, packageName,templateExtension);
        I18nTemplateDirectoryReader reader = new I18nTemplateDirectoryReader(directory, false);
        return read(definitions, encoding, reader);
    }

    private static List<I18nTemplate> read(List<I18nTemplateDefinition> definitions, Charset encoding, I18nTemplateReader reader) throws I18nResourceInitializationException {
        List<I18nTemplate> result = new LinkedList<>();
        for (I18nTemplateDefinition definition : definitions) {
            String templates = reader.read(definition, encoding);
            result.add(new I18nTemplate(definition, templates));
        }
        return result;
    }

    public I18nTemplateDefinition getDefinition() {
        return definition;
    }

    public String getTemplate() {
        return template;
    }

}
