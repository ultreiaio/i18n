package io.ultreia.java4all.i18n.spi.builder;

/*-
 * #%L
 * I18n :: Spi Builder
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Path;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface I18nTranslationSetWriter {

    void write(I18nTranslationSet translation, Path target, Charset encoding) throws IOException;

}
