package io.ultreia.java4all.i18n.spi.builder;

/*-
 * #%L
 * I18n :: Spi Builder
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;
import io.ultreia.java4all.i18n.spi.I18nProperties;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;
import io.ultreia.java4all.i18n.spi.io.I18nTranslationSetClassPathReader;
import io.ultreia.java4all.i18n.spi.io.I18nTranslationSetDirectoryReader;
import io.ultreia.java4all.i18n.spi.io.I18nTranslationSetReader;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by tchemit on 29/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTranslationSet {

    private final I18nTranslationSetDefinition definition;
    private final I18nProperties translations;

    I18nTranslationSet(I18nTranslationSetDefinition definition, Properties properties) {
        this.definition = Objects.requireNonNull(definition);
        if (properties instanceof I18nProperties) {
            this.translations = (I18nProperties) properties;
        } else {
            this.translations = new I18nProperties(StandardCharsets.UTF_8.name(), true);
            this.translations.putAll(Objects.requireNonNull(properties));
        }
    }

    static List<I18nTranslationSet> detect(ClassLoader classLoader, Charset encoding, List<I18nModuleDefinition> i18nModuleDefinitions) throws I18nResourceInitializationException {
        List<I18nTranslationSetDefinition> definitions = new LinkedList<>();
        for (I18nModuleDefinition i18nModuleDefinition : i18nModuleDefinitions) {
            definitions.addAll(i18nModuleDefinition.getModuleTranslationSetDefinitions());
        }
        I18nTranslationSetClassPathReader reader = new I18nTranslationSetClassPathReader(classLoader, true);
        return read(definitions, encoding, reader);
    }

    static List<I18nTranslationSet> detect(Path directory, String packageName, Charset encoding) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        List<I18nTranslationSetDefinition> definitions = I18nTranslationSetDefinition.detect(directory, packageName);
        I18nTranslationSetDirectoryReader reader = new I18nTranslationSetDirectoryReader(directory, false);
        return read(definitions, encoding, reader);
    }

    private static List<I18nTranslationSet> read(List<I18nTranslationSetDefinition> definitions, Charset encoding, I18nTranslationSetReader reader) throws I18nResourceInitializationException {
        List<I18nTranslationSet> result = new LinkedList<>();
        for (I18nTranslationSetDefinition definition : definitions) {
            Properties translations = reader.read(definition, encoding);
            result.add(new I18nTranslationSet(definition, translations));
        }
        return result;
    }

    public I18nTranslationSetDefinition getDefinition() {
        return definition;
    }

    public I18nProperties getTranslations() {
        return translations;
    }

}
