package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Locale;
import java.util.Objects;

/**
 * Describe any spi objects that is a localized resource (translation, or template).
 * <p>
 * Created by tchemit on 03/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nLocalizedResource extends I18nResource {

    /**
     * Locale of the localized resource.
     */
    private final Locale locale;

    public I18nLocalizedResource(String packageName, String name, String extension, Locale locale) {
        super(Objects.requireNonNull(packageName), Objects.requireNonNull(name), Objects.requireNonNull(extension));
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }

    public String getResourcePath(boolean usePackage) {
        return getResourcePathPrefix(usePackage) + getName() + "_" + locale + getExtension();
    }

}
