package io.ultreia.java4all.i18n.spi.io;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTranslationSetClassPathReader implements I18nTranslationSetReader {

    private final ClassLoader classLoader;
    private final boolean usePackage;

    public I18nTranslationSetClassPathReader(ClassLoader classLoader, boolean usePackage) {
        this.classLoader = Objects.requireNonNull(classLoader);
        this.usePackage = usePackage;
    }

    @Override
    public boolean isUsePackage() {
        return usePackage;
    }

    @Override
    public Properties read(I18nTranslationSetDefinition definition, Charset encoding) throws I18nResourceInitializationException {
        String path = I18nTranslationSetDefinition.I18N_CLASS_PATH + "/" + definition.getResourcePath(this.usePackage);
        URL resource = Objects.requireNonNull(classLoader).getResource(Objects.requireNonNull(path));
        Properties result = new Properties();
        if (resource == null) {
            throw new I18nResourceInitializationException("Could not find resource: " + path);
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.openStream(), encoding))) {
            result.load(reader);
        } catch (IOException e) {
            throw new I18nResourceInitializationException("Could not load resource: " + path, e);
        }
        return result;
    }

}
