package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;

/**
 * Describe any spi objects that is a resource (key, translation, or template).
 * <p>
 * Created by tchemit on 03/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nResource extends I18nCoordinate {

    /**
     * Where to store any i18n resources in the class-path.
     */
    protected static final String I18N_CLASS_PATH = "META-INF/i18n";
    /**
     * Extension of the resource.
     */
    private final String extension;

    public I18nResource(I18nCoordinate coordinate, String extension) {
        this(Objects.requireNonNull(coordinate).getPackageName(), coordinate.getName(), extension);
    }

    public I18nResource(String packageName, String name, String extension) {
        super(Objects.requireNonNull(packageName), removeExtension(Objects.requireNonNull(name), Objects.requireNonNull(extension)));
        this.extension = extension;
    }

    public static String removeExtension(String name, String extension) {
        if (name.endsWith(extension)) {
            name = name.substring(0, name.length() - extension.length());
        }
        return name;
    }

    public String getResourcePath(boolean usePackage) {
        return getResourcePathPrefix(usePackage) + getName() + getExtension();
    }

    public String getExtension() {
        return extension;
    }

}
