package io.ultreia.java4all.i18n.spi.io;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTranslationSetDefinition;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.Properties;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTranslationSetDirectoryReader implements I18nTranslationSetReader {

    private final Path directory;
    private final boolean usePackage;

    public I18nTranslationSetDirectoryReader(Path directory, boolean usePackage) {
        this.directory = Objects.requireNonNull(directory);
        this.usePackage = usePackage;
    }

    @Override
    public boolean isUsePackage() {
        return usePackage;
    }

    @Override
    public Properties read(I18nTranslationSetDefinition definition, Charset encoding) throws I18nResourceInitializationException {
        String fileName = definition.getResourcePath(this.usePackage);
        Path file = directory.resolve(fileName);
        Properties result = new Properties();
        if (Files.exists(file)) {
            try {
                try (BufferedReader reader = Files.newBufferedReader(file, encoding)) {
                    result.load(reader);
                }
                return result;
            } catch (IOException e) {
                throw new I18nResourceInitializationException("Can't read translations from file: " + file, e);
            }
        }
        return result;
    }

}
