package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Describe a set of templates.
 * <p>
 * Created by tchemit on 04/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTemplateDefinition extends I18nLocalizedResource {
    private static final Logger log = LogManager.getLogger(I18nTemplateDefinition.class);

    /**
     * Default extension of a i18n template file.
     */
    public static final String DEFAULT_TEMPLATE_SET_EXTENSION = ".ftl";
    /**
     * Path where to store templates.
     */
    public static final String PATH = "templates";
    /**
     * Where to store any i18n templates in the class-path.
     */
    public static final String I18N_CLASS_PATH = I18nResource.I18N_CLASS_PATH + "/" + PATH;


    public I18nTemplateDefinition(String packageName, String name, String templateExtension, Locale locale) {
        super(packageName, name, templateExtension, locale);
    }

    public static List<I18nTemplateDefinition> detect(Path directory, String packageName, String templateExtension) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        try {
            return Files
                    .walk(directory, 1)
                    .filter(p -> Files.isRegularFile(p) && p.toFile().getName().endsWith(templateExtension))
                    .map(f -> I18nTemplateDefinition.templateFromFilename(packageName, templateExtension, f))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new I18nResourceInitializationException("Could not init from: " + directory, e);
        }
    }

    public static Path write(I18nTemplateDefinition definition, Path directory, Charset encoding, boolean usePackage, String template, boolean override) throws IOException {
        if (!Files.exists(Objects.requireNonNull(directory))) {
            Files.createDirectories(directory);
        }
        String fileName = definition.getResourcePath(usePackage);
        Path target = directory.resolve(fileName);
        if (override || !Files.exists(target)) {
            log.info("Store template " + definition.getId() + " to " + target);
            Files.write(target, Collections.singletonList(template), encoding);
        }
        return target;
    }

    public static List<I18nTemplateDefinition> templateListFromCoordinate(String templateExtension, String coordinate) {
        List<I18nTemplateDefinition> result = new LinkedList<>();
        String[] tokenizer = coordinate.split(GROUP_ID_SEPARATOR);
        String packageName = tokenizer[0];
        String name = tokenizer[1];
        for (int i = 2; i < tokenizer.length; i++) {
            String locale = tokenizer[i];
            result.add(new I18nTemplateDefinition(packageName, name, templateExtension, I18nLocaleHelper.newLocale(locale)));
        }
        return result;
    }

    public static I18nTemplateDefinition templateFromFilename(String packageName, String templateExtension, Path coordinate) {
        String filename = I18nResource.removeExtension(Objects.requireNonNull(coordinate).toFile().getName(), templateExtension);
        int i = filename.indexOf("_");
        String name = filename.substring(0, i);
        String localeStr = filename.substring(i + 1);
        return new I18nTemplateDefinition(packageName, name, templateExtension, I18nLocaleHelper.newLocale(localeStr));
    }
}
