package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * Created by tchemit on 03/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nLocaleHelper {

    public static final Locale DEFAULT_LOCALE = Locale.UK;
    private static final Logger log = LogManager.getLogger(I18nLocaleHelper.class);

    /**
     * Parse a list of {@link Locale} seperated by comma.
     * <p>
     * Example : fr_FR,en_GB
     *
     * @param str the string representation of locale separated by comma
     * @return set of available locales
     * @throws IllegalArgumentException ia a locale is not valid
     */
    public static Set<Locale> parseLocalesAsSet(String str) throws IllegalArgumentException {
        return new LinkedHashSet<>(Arrays.asList(parseLocales(str)));
    }

    /**
     * Parse a list of {@link Locale} seperated by comma.
     * <p>
     * Example : fr_FR,en_GB
     *
     * @param str the string representation of locale separated by comma
     * @return list of available locales
     * @throws IllegalArgumentException ia a locale is not valid
     */
    public static Locale[] parseLocales(String str) throws IllegalArgumentException {
        List<Locale> result = new ArrayList<>();
        String[] bundlesToUse = str.split(",");
        for (String aBundlesToUse : bundlesToUse) {
            String s = aBundlesToUse.trim();
            // on devrait verifier que le bundle existe
            try {
                Locale l = LocaleConverter.valueOf(s);
                result.add(l);
            } catch (Exception e) {
                throw new IllegalArgumentException(String.format("bundle %s is not a valid locale", s), e);
            }
        }
        return result.toArray(new Locale[0]);
    }

    public static Locale newLocale(String str) {
        if (str == null) {

            // get user locale
            return newLocale(null, null);
        }
        try {
            return LocaleConverter.valueOf(str);
        } catch (Exception e) {
            if (log.isWarnEnabled()) {
                log.warn(String.format("could not load locale '%s for reason : %s", str, e.getMessage()));
            }

            // use default locale
            return DEFAULT_LOCALE;
        }
    }

    public static Locale newLocale(String language, String country) {
        if (language == null) {
            // get user locale
            language = System.getProperty("user.language", DEFAULT_LOCALE.getLanguage());
            country = System.getProperty("user.country", DEFAULT_LOCALE.getCountry());
        }
        return newLocale(language + (country == null ? "" : '_' + country));
    }
}
