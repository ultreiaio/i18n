package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Describe a set of i18n keys used by a module.
 * <p>
 * Created by tchemit on 26/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nKeySetDefinition extends I18nResource {

    /**
     * Path where to store key sets.
     */
    public static final String PATH = "getters";
    /**
     * Where to store any i18n key sets in the class-path.
     */
    public static final String I18N_CLASS_PATH = I18nResource.I18N_CLASS_PATH + "/" + PATH;

    public static String KEY_SET_EXTENSION = ".getter";

    public I18nKeySetDefinition(String packageName, String name) {
        super(packageName, name, KEY_SET_EXTENSION);
    }

    public I18nKeySetDefinition(I18nCoordinate coordinate) {
        super(coordinate, KEY_SET_EXTENSION);
    }

    public static I18nKeySetDefinition newKey(String coordinate) {
        String[] split = Objects.requireNonNull(coordinate).split(GROUP_ID_SEPARATOR);
        return new I18nKeySetDefinition(split[0], split[1]);
    }

    public static I18nKeySetDefinition newKeyFromPath(String packageName, Path coordinate) {
        return newKey(packageName + GROUP_ID_SEPARATOR + removeExtension(coordinate.toFile().getName(), KEY_SET_EXTENSION));
    }

    public static List<I18nKeySetDefinition> detect(Path directory, String packageName) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        String extension = I18nKeySetDefinition.KEY_SET_EXTENSION;

        try {
            return Files
                    .walk(directory, 1)
                    .filter(p -> Files.isRegularFile(p) && p.toFile().getName().endsWith(extension))
                    .map(f -> I18nKeySetDefinition.newKeyFromPath(packageName, f))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new I18nResourceInitializationException("Can't detect key sets from directory: " + directory, e);
        }
    }

    public static Path write(I18nKeySetDefinition definition, Path directory, Charset encoding, boolean usePackage, Set<String> keys) throws IOException {
        if (!Files.exists(Objects.requireNonNull(directory))) {
            Files.createDirectories(directory);
        }
        String fileName = definition.getResourcePath(usePackage);
        Path target = directory.resolve(fileName);
        Files.write(target, keys, encoding);
        return target;
    }
}
