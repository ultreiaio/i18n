package io.ultreia.java4all.i18n.spi.io;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nKeySetDefinition;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nResourceNotFoundException;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nKeySetClassPathReader implements I18nKeySetReader {

    private final ClassLoader classLoader;
    private final boolean usePackage;

    public I18nKeySetClassPathReader(ClassLoader classLoader, boolean usePackage) {
        this.classLoader = Objects.requireNonNull(classLoader);
        this.usePackage = usePackage;
    }

    @Override
    public boolean isUsePackage() {
        return usePackage;
    }

    @Override
    public Set<String> read(I18nKeySetDefinition definition) throws I18nResourceInitializationException {
        String path = I18nKeySetDefinition.I18N_CLASS_PATH + "/" + definition.getResourcePath(this.usePackage);
        Set<String> keys = new TreeSet<>();
        try {
            URL resource = Objects.requireNonNull(classLoader).getResource(Objects.requireNonNull(path));
            if (resource == null) {
                throw new I18nResourceNotFoundException("Could not find resource: " + path);
            }
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.openStream(), StandardCharsets.UTF_8))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    keys.add(line);
                }
            }
        } catch (I18nResourceNotFoundException e) {
            throw e;
        } catch (Exception e) {
            throw new I18nResourceInitializationException("Could not load keys from classpath: " + path, e);
        }
        return keys;
    }

}
