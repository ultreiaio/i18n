package io.ultreia.java4all.i18n.spi.enumeration;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * To add a translation for an enumeration.
 *
 * Created on 05/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.0
 */
@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.TYPE)
@Documented
public @interface TranslateEnumeration {
    /**
     * @return name of the translation definition
     */
    String name();

    /**
     * Content can use :
     * <ul>
     * <li>{@code @CLASS_NAME@} for enumeration class fully qualified name</li>
     * <li>{@code @CLASS_SIMPLE_NAME@} for enumeration class simple name</li>
     * <li>{@code @NAME@} for enumeration name</li>
     * <li>{@code @ORDINAL@} for enumeration ordinal</li>
     * </ul>
     *
     * @return the pattern to apply to get the i18n key
     */
    String pattern();
}
