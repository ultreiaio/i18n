package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created at 13/02/2024.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.0
 */
public class LocaleConverter {

    private static final Pattern FULL_SCOPE_PATTERN =
            Pattern.compile("([a-zA-Z]{2})_([a-zA-Z]{2})");

    private static final Pattern MEDIUM_SCOPE_PATTERN =
            Pattern.compile("([a-zA-Z]{2})");

    public static Locale valueOf(String value) {

        Locale result = convertFullScope(value);

        if (result == null) {
            result = convertMediumScope(value);
        }

        if (result == null) {
            throw new IllegalStateException("could not convert locale " + value);
        }

        return result;

    }

    private static Locale convertFullScope(String value) {
        Matcher m = FULL_SCOPE_PATTERN.matcher(value);
        if (m.matches()) {
            // found a full scope pattern (language + country)
            String language = m.group(1).toLowerCase();
            String country = m.group(2).toUpperCase();

            return new Locale(language, country);
        }
        return null;
    }

    private static Locale convertMediumScope(String value) {
        Matcher m = MEDIUM_SCOPE_PATTERN.matcher(value);
        if (m.matches()) {

            // found a medium scope pattern (only language)
            String language = m.group(1).toLowerCase();

            return new Locale(language);
        }
        return null;
    }

}
