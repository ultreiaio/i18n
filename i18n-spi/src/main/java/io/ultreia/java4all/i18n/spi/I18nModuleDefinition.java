package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Describe a i18n module.
 * <p>
 * For each maven module using i18n mojo, a implementation of this class wil be generated and added as service.
 * <p>
 * Created by tchemit on 25/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nModuleDefinition extends I18nCoordinate {

    /**
     * Module key sets.
     */
    private final List<I18nKeySetDefinition> moduleKeySetDefinitions;
    /**
     * Dependencies key sets.
     */
    private final List<I18nKeySetDefinition> dependenciesKeySetDefinitions;
    /**
     * Module translation sets.
     */
    private final List<I18nTranslationSetDefinition> moduleTranslationSetDefinitions;
    /**
     * Dependencies translation sets.
     */
    private final List<I18nTranslationSetDefinition> dependenciesTranslationSetDefinitions;
    /**
     * Module template sets.
     */
    private final List<I18nTemplateDefinition> moduleTemplateSetDefinitions;
    /**
     * Dependencies template sets.
     */
    private final List<I18nTemplateDefinition> dependenciesTemplateSetDefinitions;
    /**
     * Template extension.
     */
    private final String templateExtension;

    public I18nModuleDefinition(String packageName,
                                String moduleName,
                                String[] moduleGetters,
                                String[] dependenciesGetters,
                                String[] moduleTranslations,
                                String[] dependenciesTranslations,
                                String[] moduleTemplates,
                                String[] dependenciesTemplates) {
        this(packageName, moduleName, I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION, moduleGetters, dependenciesGetters, moduleTranslations, dependenciesTranslations, moduleTemplates, dependenciesTemplates);
    }

    public I18nModuleDefinition(String packageName,
                                String moduleName,
                                String templateExtension,
                                String[] moduleGetters,
                                String[] dependenciesGetters,
                                String[] moduleTranslations,
                                String[] dependenciesTranslations,
                                String[] moduleTemplates,
                                String[] dependenciesTemplates) {
        super(packageName, moduleName);
        this.templateExtension = templateExtension;
        this.moduleKeySetDefinitions = new LinkedList<>();
        this.dependenciesKeySetDefinitions = new LinkedList<>();
        this.moduleTranslationSetDefinitions = new LinkedList<>();
        this.dependenciesTranslationSetDefinitions = new LinkedList<>();
        this.moduleTemplateSetDefinitions = new LinkedList<>();
        this.dependenciesTemplateSetDefinitions = new LinkedList<>();

        Arrays.stream(moduleGetters).map(I18nKeySetDefinition::newKey).forEachOrdered(this.moduleKeySetDefinitions::add);
        Arrays.stream(dependenciesGetters).map(I18nKeySetDefinition::newKey).forEachOrdered(this.dependenciesKeySetDefinitions::add);

        Arrays.stream(moduleTranslations).map(I18nTranslationSetDefinition::translationSetListFromCoordinate).forEachOrdered(this.moduleTranslationSetDefinitions::addAll);
        Arrays.stream(dependenciesTranslations).map(I18nTranslationSetDefinition::translationSetListFromCoordinate).forEachOrdered(this.dependenciesTranslationSetDefinitions::addAll);

        Arrays.stream(moduleTemplates).map(f -> I18nTemplateDefinition.templateListFromCoordinate(templateExtension, f)).forEachOrdered(this.moduleTemplateSetDefinitions::addAll);
        Arrays.stream(dependenciesTemplates).map(f -> I18nTemplateDefinition.templateListFromCoordinate(templateExtension, f)).forEachOrdered(this.dependenciesTemplateSetDefinitions::addAll);
    }

    public static List<I18nModuleDefinition> detect(ClassLoader classLoader) {
        List<I18nModuleDefinition> result = new LinkedList<>();
        for (I18nModuleDefinition i18nModuleDefinition : ServiceLoader.load(I18nModuleDefinition.class, classLoader)) {
            result.add(i18nModuleDefinition);
        }
        return result;
    }

    public String getTemplateExtension() {
        return templateExtension;
    }

    public boolean isContainsModuleKeySets() {
        return !moduleKeySetDefinitions.isEmpty();
    }

    public boolean isContainsDependenciesKeySets() {
        return !dependenciesKeySetDefinitions.isEmpty();
    }

    public boolean isContainsModuleTemplateSets() {
        return !moduleTemplateSetDefinitions.isEmpty();
    }

    public boolean isContainsDependenciesTemplateSets() {
        return !dependenciesTemplateSetDefinitions.isEmpty();
    }

    public boolean isContainsModuleTranslationSets() {
        return !moduleTranslationSetDefinitions.isEmpty();
    }

    public boolean isContainsDependenciesTranslationSets() {
        return !dependenciesTranslationSetDefinitions.isEmpty();
    }

    public List<I18nKeySetDefinition> getModuleKeySetDefinitions() {
        return moduleKeySetDefinitions;
    }

    public List<I18nKeySetDefinition> getDependenciesKeySetDefinitions() {
        return dependenciesKeySetDefinitions;
    }

    public List<I18nTranslationSetDefinition> getModuleTranslationSetDefinitions() {
        return moduleTranslationSetDefinitions;
    }

    public Set<Locale> getModuleTranslationSetLocales() {
        return moduleTranslationSetDefinitions.stream().map(I18nLocalizedResource::getLocale).collect(Collectors.toSet());
    }

    public List<I18nTranslationSetDefinition> getDependenciesTranslationSetDefinitions() {
        return dependenciesTranslationSetDefinitions;
    }

    public List<I18nTemplateDefinition> getModuleTemplateSetDefinitions() {
        return moduleTemplateSetDefinitions;
    }

    public List<I18nTemplateDefinition> getDependenciesTemplateSetDefinitions() {
        return dependenciesTemplateSetDefinitions;
    }

    @Override
    public String toString() {
        String moduleKeySetDefinitions = getModuleKeySetDefinitions().stream().map(I18nKeySetDefinition::getId).collect(Collectors.joining(" "));
        String dependenciesKeySetDefinitions = getDependenciesKeySetDefinitions().stream().map(I18nKeySetDefinition::getId).collect(Collectors.joining(" "));
        String moduleTemplateSetDefinitions = getModuleTemplateSetDefinitions().stream().map(I18nTemplateDefinition::getId).collect(Collectors.joining(" "));
        String dependenciesTemplateSetDefinitions = getDependenciesTemplateSetDefinitions().stream().map(I18nTemplateDefinition::getId).collect(Collectors.joining(" "));
        String moduleTranslationDefinitions = getModuleTranslationSetDefinitions().stream().map(I18nTranslationSetDefinition::getId).map(Object::toString).collect(Collectors.joining(" "));
        String dependenciesTranslationDefinitions = getDependenciesTranslationSetDefinitions().stream().map(I18nTranslationSetDefinition::getId).map(Object::toString).collect(Collectors.joining(" "));
        return "I18nModuleDefinition{" +
                "moduleId='" + getId() + '\'' +
                (moduleKeySetDefinitions.isEmpty() ? "" : "module.keySets='" + moduleKeySetDefinitions + '\'') +
                (dependenciesKeySetDefinitions.isEmpty() ? "" : "dependencies.keySets='" + dependenciesKeySetDefinitions + '\'') +
                (moduleTranslationDefinitions.isEmpty() ? "" : "module.translation='" + moduleTranslationDefinitions + '\'') +
                (dependenciesTranslationDefinitions.isEmpty() ? "" : "dependencies.translation='" + dependenciesTranslationDefinitions + '\'') +
                (moduleTemplateSetDefinitions.isEmpty() ? "" : "module.templateSets='" + moduleTemplateSetDefinitions + '\'') +
                (dependenciesTemplateSetDefinitions.isEmpty() ? "" : "dependencies.templateSets='" + dependenciesTemplateSetDefinitions + '\'') +
                '}';
    }

    public Set<I18nTranslationSetDefinition> getModuleTranslationSetDefinitions(Locale locale) {
        return moduleTranslationSetDefinitions.stream().filter(d -> locale.equals(d.getLocale())).collect(Collectors.toSet());
    }

    public Set<I18nTemplateDefinition> getModuleTemplateSetDefinitions(Locale locale) {
        return moduleTemplateSetDefinitions.stream().filter(d -> locale.equals(d.getLocale())).collect(Collectors.toSet());
    }
}
