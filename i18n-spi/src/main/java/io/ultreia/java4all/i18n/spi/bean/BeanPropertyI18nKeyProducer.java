package io.ultreia.java4all.i18n.spi.bean;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Produce i18n key for any property of a bean.
 * <p>
 * This should be used at application level to make easy translations of your objects.
 * <p>
 * Created by tchemit on 18/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface BeanPropertyI18nKeyProducer {

    /**
     * Get the i18n property key for the given {@code property} of the given {@code type}.
     *
     * @param type     type of object
     * @param property property name
     * @return i18n property key (not translated)
     */
    String getI18nPropertyKey(Class<?> type, String property);

    /**
     * Get the i18n key for the given {@code type}.
     *
     * @param type type to use
     * @return i18n type key (not translated)
     */
    String getI18nTypeKey(Class<?> type);

    /**
     * Map the property (used in {@link #getI18nPropertyKey(Class, String)}.
     *
     * @param property property to map
     * @return property mapping found or the incoming value if not specialized.
     */
    String getPropertyMapping(String property);

    String getCommonPrefix();
}
