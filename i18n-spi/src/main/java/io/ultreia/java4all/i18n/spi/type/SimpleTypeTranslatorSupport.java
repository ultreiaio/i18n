package io.ultreia.java4all.i18n.spi.type;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Simple implementation of {@link TypeTranslator} using a {@link #rootPackage} and a {@link #type}.
 * <p>
 * Created on 07/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.0
 */
public abstract class SimpleTypeTranslatorSupport implements TypeTranslator {

    /**
     * Root package used to perform translation.
     */
    private final String rootPackage;
    /**
     * Ancestor of all accepted type by this type translator.
     */
    private final Class<?> type;
    /**
     * All suffixes to remove (order by bigger to smaller in order to not do semi-translations...)
     */
    private final Set<String> suffixes;

    protected SimpleTypeTranslatorSupport(String rootPackage, Class<?> type, String... suffixes) {
        this.rootPackage = Objects.requireNonNull(rootPackage);
        this.type = Objects.requireNonNull(type);
        this.suffixes = Arrays.stream(suffixes).sorted(Comparator.comparing(String::toString).thenComparingInt(String::length).reversed()).collect(Collectors.toCollection(LinkedHashSet::new));
    }

    public String getRootPackage() {
        return rootPackage;
    }

    public Class<?> getType() {
        return type;
    }

    public Set<String> getSuffixes() {
        return suffixes;
    }

    @Override
    public boolean acceptType(Class<?> type) {
        return getType().isAssignableFrom(type) && type.getPackageName().startsWith(getRootPackage());
    }

    @Override
    public String cleanType(Class<?> type) {
        return getRootPackage().equals(type.getPackageName()) ? cleanSimpleName(type.getSimpleName()) : cleanType(type.getName());
    }

    @Override
    public String cleanType(String fqn) {
        int lastIndex = fqn.lastIndexOf('.');
        return fqn.substring(getRootPackage().length() + 1, lastIndex) + "." + cleanSimpleName(fqn.substring(lastIndex + 1));
    }

    @Override
    public String cleanSimpleName(String simpleName) {
        for (String suffix : getSuffixes()) {
            int i = simpleName.lastIndexOf(suffix);
            if (i == -1) {
                continue;
            }
            simpleName = simpleName.substring(0, i);

        }
        return simpleName;
    }

}
