package io.ultreia.java4all.i18n.spi.bean;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.type.TypeTranslators;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Created by tchemit on 18/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BeanPropertyI18nKeyProducerSupport implements BeanPropertyI18nKeyProducer {

    private final String commonPrefix;
    private final Map<String, String> mapping;
    private final Map<String, String> propertyMapping;

    private final String labelsLocation;
    private final ClassLoader classLoader;

    protected BeanPropertyI18nKeyProducerSupport(String commonPrefix, String projectName, ClassLoader classLoader) {
        this.commonPrefix = commonPrefix;
        this.labelsLocation = String.format("META-INF/i18n/%s-labels.properties", Objects.requireNonNull(projectName));
        this.classLoader = Objects.requireNonNull(classLoader);
        URL resource = classLoader.getResource(labelsLocation);
        Objects.requireNonNull(resource, String.format("Can't find resource: %s", labelsLocation));
        this.mapping = createMapping();
        this.propertyMapping = createPropertyKeyMapping();
    }

    @Override
    public String getCommonPrefix() {
        return commonPrefix;
    }

    @Override
    public String getI18nPropertyKey(Class<?> type, String property) {
        // translate property name if necessary
        String propertyMapping = getPropertyMapping(property);
        // request key
        String key = commonPrefix + getI18nTypeKey(type) + "." + propertyMapping;
        // translate key or return it if no mapping found
        return mapping.getOrDefault(key, key);
    }

    @Override
    public String getPropertyMapping(String property) {
        return propertyMapping.getOrDefault(property, property);
    }

    @Override
    public String getI18nTypeKey(Class<?> type) {
        return TypeTranslators.getSimplifiedName(type);
    }

    protected Map<String, String> createMapping() {
        URL resource = classLoader.getResource(labelsLocation);
        Map<String, String> result = new TreeMap<>();
        try (InputStream inputStream = Objects.requireNonNull(resource, String.format("Can't find resource: %s", labelsLocation)).openStream()) {
            Properties p = new Properties();
            p.load(inputStream);
            p.forEach((key, value) -> result.put(key.toString(), value.toString()));
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't load resource: %s", labelsLocation), e);
        }
        return result;
    }

    protected Map<String, String> createPropertyKeyMapping() {
        return Collections.emptyMap();
    }

}
