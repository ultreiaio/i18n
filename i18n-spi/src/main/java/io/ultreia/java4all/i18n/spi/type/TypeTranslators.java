package io.ultreia.java4all.i18n.spi.type;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * {@link TypeTranslator} store.
 * <p>
 * Created on 07/08/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 4.0.0
 */
public class TypeTranslators {

    private static TypeTranslators INSTANCE;
    private final Set<TypeTranslator> translators;

    public static TypeTranslators get() {
        return INSTANCE == null ? INSTANCE = new TypeTranslators() : INSTANCE;
    }

    public static String getSimplifiedName(Class<?> dataType) {
        Objects.requireNonNull(dataType, " Null type given");
        return getTranslator(dataType).cleanType(dataType);
    }

    public static TypeTranslator getTranslator(Class<?> type) {
        return optionalTranslator(type).orElseThrow(() -> new IllegalStateException("Can't type type translator for type: " + type));
    }

    private static Optional<TypeTranslator> optionalTranslator(Class<?> type) {
        //TODO Use a LoadingCache
        return get().getTranslators().stream().filter(t -> t.acceptType(type)).findFirst();
    }

    public TypeTranslators() {
        translators = ServiceLoader.load(TypeTranslator.class).stream().map(ServiceLoader.Provider::get).collect(Collectors.toSet());
    }

    public Set<TypeTranslator> getTranslators() {
        return translators;
    }
}
