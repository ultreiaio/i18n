package io.ultreia.java4all.i18n.spi.io;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.spi.I18nKeySetDefinition;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nKeySetDirectoryReader implements I18nKeySetReader {

    private final Path directory;
    private final boolean usePackage;

    public I18nKeySetDirectoryReader(Path directory, boolean usePackage) {
        this.directory = Objects.requireNonNull(directory);
        this.usePackage = usePackage;
    }

    @Override
    public boolean isUsePackage() {
        return usePackage;
    }

    @Override
    public Set<String> read(I18nKeySetDefinition definition) throws I18nResourceInitializationException {
        String fileName = definition.getResourcePath(this.usePackage);
        Path file = directory.resolve(fileName);
        if (Files.exists(file)) {
            try {
                List<String> keys = Files.readAllLines(file, StandardCharsets.UTF_8);
                return new TreeSet<>(keys);
            } catch (IOException e) {
                throw new I18nResourceInitializationException("Can't load keySet from file: " + file, e);
            }
        }
        return new TreeSet<>();
    }

}
