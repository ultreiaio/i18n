package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Describe a i18n translation resource.
 * <p>
 * Created by tchemit on 26/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTranslationSetDefinition extends I18nLocalizedResource {

    /**
     * Extension of a translation set file.
     */
    public static final String TRANSLATION_SET_EXTENSION = ".properties";
    /**
     * Path where to store translation sets.
     */
    public static final String PATH = "translations";
    /**
     * Where to store any i18n translation sets in the class-path.
     */
    public static final String I18N_CLASS_PATH = I18nResource.I18N_CLASS_PATH + "/" + PATH;
    private static final Logger log = LogManager.getLogger(I18nTranslationSetDefinition.class);

    public I18nTranslationSetDefinition(String packageName, String name, Locale locale) {
        super(packageName, name, TRANSLATION_SET_EXTENSION, locale);
    }

    public static List<I18nTranslationSetDefinition> translationSetListFromCoordinate(String coordinate) {
        List<I18nTranslationSetDefinition> result = new LinkedList<>();
        String[] tokenizer = coordinate.split(GROUP_ID_SEPARATOR);
        String packageName = tokenizer[0];
        String name = tokenizer[1];
        for (int i = 2; i < tokenizer.length; i++) {
            String locale = tokenizer[i];
            result.add(new I18nTranslationSetDefinition(packageName, name, I18nLocaleHelper.newLocale(locale)));
        }
        return result;
    }

    public static I18nTranslationSetDefinition translationSetFromFilename(String packageName, Path coordinate) {
        String filename = I18nResource.removeExtension(Objects.requireNonNull(coordinate).toFile().getName(), TRANSLATION_SET_EXTENSION);
        int i = filename.indexOf("_");
        String name = filename.substring(0, i);
        String localeStr = filename.substring(i + 1);
        return new I18nTranslationSetDefinition(packageName, name, I18nLocaleHelper.newLocale(localeStr));
    }

    public static Path write(I18nTranslationSetDefinition definition, Charset encoding, boolean usePackage, Path directory, Properties properties, boolean override) throws IOException {
        if (!Files.exists(Objects.requireNonNull(directory))) {
            Files.createDirectories(directory);
        }
        String fileName = definition.getResourcePath(usePackage);
        Path target = directory.resolve(fileName);
        if (override || !Files.exists(target)) {
            I18nProperties p;
            if (properties instanceof I18nProperties) {
                p = (I18nProperties) properties;
            } else {
                p = new I18nProperties(encoding.name(), true);
            }
            p.putAll(properties);
            log.info("Store translations " + definition.getId() + " to " + target);
            p.store(target.toFile());
        }
        return target;
    }

    public static List<I18nTranslationSetDefinition> detect(Path directory, String packageName) throws I18nResourceInitializationException {
        if (!Files.exists(directory)) {
            return Collections.emptyList();
        }
        try {
            return Files
                    .walk(directory, 1)
                    .filter(p -> Files.isRegularFile(p) && p.toFile().getName().endsWith(I18nTranslationSetDefinition.TRANSLATION_SET_EXTENSION))
                    .map(f -> I18nTranslationSetDefinition.translationSetFromFilename(packageName, f))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new I18nResourceInitializationException("Could not init from: " + directory, e);
        }
    }
}
