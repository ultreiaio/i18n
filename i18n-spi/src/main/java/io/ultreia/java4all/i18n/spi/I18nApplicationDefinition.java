package io.ultreia.java4all.i18n.spi;

/*-
 * #%L
 * I18n :: Spi
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Describe a i18n application definition.
 * <p>
 * Created by tchemit on 31/10/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@SuppressWarnings("WeakerAccess")
public class I18nApplicationDefinition extends I18nCoordinate {

    /**
     * Encoding used to load data.
     */
    private final Charset encoding;
    /**
     * Application version.
     */
    private final String version;
    /**
     * Application locales.
     */
    private final List<Locale> locales;
    /**
     * Dictionary of categorized keys.
     */
    private final Map<String, String> keyCategories;
    /**
     * Main translations.
     */
    private final List<I18nTranslationSetDefinition> applicationTranslationDefinitions;
    /**
     * Dependencies translations.
     */
    private final List<I18nTranslationSetDefinition> dependenciesTranslationDefinitions;
    /**
     * Application templates.
     */
    private final List<I18nTemplateDefinition> applicationTemplateDefinitions;

    /**
     * Template extension.
     */
    private final String templateExtension;

    public I18nApplicationDefinition(String packageName,
                                     String name,
                                     Charset encoding,
                                     String version,
                                     String[] locales,
                                     Map<String, String> keyCategories,
                                     String[] moduleTranslations,
                                     String[] dependenciesTranslations,
                                     String[] applicationTemplates) {
        this(packageName, name, I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION, encoding, version, locales, keyCategories, moduleTranslations, dependenciesTranslations, applicationTemplates);
    }

    public I18nApplicationDefinition(String packageName,
                                     String name,
                                     String templateExtension,
                                     Charset encoding,
                                     String version,
                                     String[] locales,
                                     Map<String, String> keyCategories,
                                     String[] moduleTranslations,
                                     String[] dependenciesTranslations,
                                     String[] applicationTemplates) {
        super(packageName, name);
        this.templateExtension = templateExtension;
        this.encoding = encoding;
        this.version = version;
        this.locales = Collections.unmodifiableList(Arrays.stream(locales).map(I18nLocaleHelper::newLocale).collect(Collectors.toList()));
        this.keyCategories = keyCategories;
        this.applicationTranslationDefinitions = new LinkedList<>();
        this.dependenciesTranslationDefinitions = new LinkedList<>();
        this.applicationTemplateDefinitions = new LinkedList<>();
        Arrays.stream(moduleTranslations).map(I18nTranslationSetDefinition::translationSetListFromCoordinate).forEachOrdered(this.applicationTranslationDefinitions::addAll);
        Arrays.stream(dependenciesTranslations).map(I18nTranslationSetDefinition::translationSetListFromCoordinate).forEachOrdered(this.dependenciesTranslationDefinitions::addAll);
        Arrays.stream(applicationTemplates).map(f -> I18nTemplateDefinition.templateListFromCoordinate(templateExtension, f)).forEachOrdered(this.applicationTemplateDefinitions::addAll);
    }

    public I18nApplicationDefinition(String packageName,
                                     String name,
                                     Charset encoding,
                                     String version,
                                     List<I18nModuleDefinition> moduleDefinitions,
                                     Map<String, String> keyCategories) {
        this(packageName, name, I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION, encoding, version, moduleDefinitions, keyCategories);
    }

    public I18nApplicationDefinition(String packageName,
                                     String name,
                                     String templateExtension,
                                     Charset encoding,
                                     String version,
                                     List<I18nModuleDefinition> moduleDefinitions,
                                     Map<String, String> keyCategories) {
        super(packageName, name);
        this.templateExtension = templateExtension;
        this.encoding = encoding;
        this.version = version;

        List<I18nTranslationSetDefinition> translationSets = new LinkedList<>();
        List<I18nTemplateDefinition> templates = new LinkedList<>();
        Set<Locale> locales = new LinkedHashSet<>();
        for (I18nModuleDefinition i18nModuleDefinition : moduleDefinitions) {
            for (I18nTranslationSetDefinition moduleTranslationSetDefinition : i18nModuleDefinition.getModuleTranslationSetDefinitions()) {
                locales.add(moduleTranslationSetDefinition.getLocale());
            }
            translationSets.addAll(i18nModuleDefinition.getModuleTranslationSetDefinitions());
            templates.addAll(i18nModuleDefinition.getModuleTemplateSetDefinitions());
        }
        this.locales = Collections.unmodifiableList(new LinkedList<>(locales));
        this.keyCategories = keyCategories;
        this.applicationTranslationDefinitions = new LinkedList<>();
        this.dependenciesTranslationDefinitions = new LinkedList<>(translationSets);
        this.applicationTemplateDefinitions = new LinkedList<>(templates);
    }

    public static Optional<I18nApplicationDefinition> detect(ClassLoader classLoader) {
        List<I18nApplicationDefinition> result = new LinkedList<>();
        for (I18nApplicationDefinition i18nModuleDefinition : ServiceLoader.load(I18nApplicationDefinition.class, classLoader)) {
            result.add(i18nModuleDefinition);
        }
        if (result.size() == 1) {
            return Optional.of(result.get(0));
        }
        return Optional.empty();
    }

    public String getTemplateExtension() {
        return templateExtension;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public String getVersion() {
        return version;
    }

    public List<Locale> getLocales() {
        return locales;
    }

    public List<I18nTranslationSetDefinition> getApplicationTranslationDefinitions() {
        return applicationTranslationDefinitions;
    }

    public List<I18nTranslationSetDefinition> getDependenciesTranslationDefinitions() {
        return dependenciesTranslationDefinitions;
    }

    public List<I18nTemplateDefinition> getApplicationTemplateDefinitions() {
        return applicationTemplateDefinitions;
    }

    public Set<String> getTemplateList() {
        return applicationTemplateDefinitions.stream().map(I18nCoordinate::getName).collect(Collectors.toSet());
    }

    public Map<String, String> getKeyCategories() {
        return keyCategories;
    }

}
