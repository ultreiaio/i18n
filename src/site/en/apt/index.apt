~~~
~~ #%L
~~ I18n :: Pom
~~ %%
~~ Copyright (C) 2008 - 2024 Code Lutin, Ultreia.io
~~ %%
~~ This program is free software: you can redistribute it and/or modify
~~ it under the terms of the GNU Lesser General Public License as
~~ published by the Free Software Foundation, either version 3 of the
~~ License, or (at your option) any later version.
~~
~~ This program is distributed in the hope that it will be useful,
~~ but WITHOUT ANY WARRANTY; without even the implied warranty of
~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
~~ GNU General Lesser Public License for more details.
~~
~~ You should have received a copy of the GNU General Lesser Public
~~ License along with this program.  If not, see
~~ <http://www.gnu.org/licenses/lgpl-3.0.html>.
~~ #L%
~~~

  ---
Nuiton I18n
  ---

Nuiton I18n

  Nuiton I18n project features Java classes for applications
  internationalisation (i18n).

  This light library combine Java ResourceBundles, a powerful and easy to use
  API and a really useful extension process. Lots of avantages comes with Nuiton
  I18n use :

    - translation keys extraction

    - build process integration through Maven or Ant

  Here is a small example that shows Nuiton I18n usage simplicity :

-----------------------------------------------------------------------------
I18n.init(new DefaultI18nInitializer("myBundle"), Locale.FR);
System.out.println(I18n._("This text will be translated"));
-----------------------------------------------------------------------------

  Have a look to the tutorials to see how to enrich your application with Nuiton
  I18n.

  The three Nuiton I18n project modules :

  - {{{./i18n-api/index.html}I18n Api}}

  - {{{./maven-i18n-plugin/index.html}I18n Maven Plugin}}

  - {{{./i18n-editor/index.html}I18n Editor}}
