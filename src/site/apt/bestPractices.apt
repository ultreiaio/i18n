~~~
~~ #%L
~~ I18n :: Pom
~~ %%
~~ Copyright (C) 2008 - 2024 Code Lutin, Ultreia.io
~~ %%
~~ This program is free software: you can redistribute it and/or modify
~~ it under the terms of the GNU Lesser General Public License as
~~ published by the Free Software Foundation, either version 3 of the
~~ License, or (at your option) any later version.
~~
~~ This program is distributed in the hope that it will be useful,
~~ but WITHOUT ANY WARRANTY; without even the implied warranty of
~~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
~~ GNU General Lesser Public License for more details.
~~
~~ You should have received a copy of the GNU General Lesser Public
~~ License along with this program.  If not, see
~~ <http://www.gnu.org/licenses/lgpl-3.0.html>.
~~ #L%
~~~

   ---
Bonnes pratiques Nuiton I18n
   ---

Bonnes pratiques Nuiton I18n

* Choisissez un pattern et suivez le.

  Dans une application ou une librairie, vous devriez choisir un pattern pour
  les clés i18n. Vous pouvez choisir d'utiliser des clés comme
  myapplication.key1 ou des phrases comme 'My application string'. Une fois que
  vous avez fait votre choix, suivez-le afin de conserver de la consistance
  dans votre application. Nottez que dans une application, vous pouvez utiliser
  un pattern différent de celui utilisé dans ses dépendances.

* Toujours utiliser DefaultI18nInitializer dans une application finale.

  Ce n'est pas une obligation, mais comme cela vous fait gagner un temps
  considérable à l'initialisation, il est fortement conseillé d'utiliser
  DefaultI18nInitializer pour toutes vos applications finales.

  De plus cet initializer basé sur une unique bundle (produit par le goal bundle)
  permet de vous assurer que les traductions de plus haut niveau (celles que
  vous avez peut-être surchargées dans votre application finale seront bien
  celles utilisés et non pas celle d'une librairie...).
  
