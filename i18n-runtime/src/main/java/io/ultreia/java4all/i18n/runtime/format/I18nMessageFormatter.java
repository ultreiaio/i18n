/*
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.i18n.runtime.format;

import java.util.Locale;

/**
 * Contract to format a i18n message.
 * <p>
 * Created: 05/05/11
 *
 * @author Florian Desbois
 * @author Tony Chemit - dev@tchemit.fr
 * @see StringFormatI18nMessageFormatter
 * @see MessageFormatI18nMessageFormatter
 * @since 2.4
 */
public interface I18nMessageFormatter {

    /**
     * Format a given {@code message} with {@code locale} and optinal {@code args}.
     * <p>
     * The main purpose is to replace placeholders in {@code message} with
     * {@code args} value.
     * <p>
     * This may depend on {@code locale} for date purpose for example.
     *
     * @param locale  locale to use for formatting (number, date, ...)
     * @param message message to format (that contains placeholders)
     * @param args    object array to use in formatting the message
     * @return the formatted message
     */
    String format(Locale locale, String message, Object... args);

}
