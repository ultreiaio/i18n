package io.ultreia.java4all.i18n.runtime.boot;

/*-
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;
import io.ultreia.java4all.i18n.spi.I18nApplicationDefinition;
import io.ultreia.java4all.i18n.spi.I18nModuleDefinition;
import io.ultreia.java4all.i18n.spi.I18nResourceInitializationException;
import io.ultreia.java4all.i18n.spi.I18nTemplateDefinition;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by tchemit on 05/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class DefaultI18nBootLoader implements I18nBootLoader {

    private final I18nConfiguration configuration;

    public DefaultI18nBootLoader(I18nConfiguration configuration) {
        this.configuration = configuration;
    }

    private static I18nApplicationDefinition create(I18nConfiguration configuration) {

        List<I18nModuleDefinition> moduleDefinitions = I18nModuleDefinition.detect(configuration.getClassLoader());
        return new I18nApplicationDefinition(
                "io.ultreia.java4all.i18n",
                "internal",
                I18nTemplateDefinition.DEFAULT_TEMPLATE_SET_EXTENSION,
                configuration.getEncoding(),
                "0",
                moduleDefinitions,
                Collections.emptyMap());
    }

    @Override
    public I18nConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public I18nLanguageProvider init(Locale locale) {

        I18nApplicationDefinition applicationDefinition = I18nApplicationDefinition.detect(configuration.getClassLoader()).orElse(create(configuration));

        try {
            return I18nLanguageProvider.forClassPath(locale, configuration, applicationDefinition);
        } catch (I18nResourceInitializationException e) {
            throw new I18nInitializationException("Could not boot i18n ", e);
        }
    }
}
