package io.ultreia.java4all.i18n.runtime;

/*-
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.runtime.format.I18nMessageFormatter;
import io.ultreia.java4all.i18n.runtime.format.StringFormatI18nMessageFormatter;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * Created by tchemit on 05/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nConfiguration {

    public static final I18nConfiguration DEFAULT_CONFIGURATION = createDefaultConfiguration();

    private boolean missingKeyReturnNull;
    private Charset encoding;
    private ClassLoader classLoader;
    private I18nMessageFormatter messageFormatter;

    public static I18nConfiguration createDefaultConfiguration() {
        I18nConfiguration configuration = new I18nConfiguration();
        configuration.setEncoding(StandardCharsets.UTF_8);
        configuration.setClassLoader(I18nConfiguration.class.getClassLoader());
        configuration.setMessageFormatter(new StringFormatI18nMessageFormatter());
        return configuration;
    }

    public I18nMessageFormatter getMessageFormatter() {
        return messageFormatter;
    }

    public void setMessageFormatter(I18nMessageFormatter messageFormatter) {
        this.messageFormatter = messageFormatter;
    }

    public boolean isMissingKeyReturnNull() {
        return missingKeyReturnNull;
    }

    public void setMissingKeyReturnNull(boolean missingKeyReturnNull) {
        this.missingKeyReturnNull = missingKeyReturnNull;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding(Charset encoding) {
        this.encoding = encoding;
    }

    public ClassLoader getClassLoader() {
        return classLoader;
    }

    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
