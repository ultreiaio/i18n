/*
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.i18n.runtime.format;

import java.text.MessageFormat;
import java.util.Locale;

/**
 * Implementation of {@link I18nMessageFormatter} based on
 * {@link MessageFormat} syntax.
 * <p>
 * <strong>Note:</strong> A formatter will always be used even if there is no
 * parameters in translation. As {@link MessageFormat} requires to espece
 * quotes, we need to do this for every translation with or without parameters.
 *
 * @see MessageFormat
 * @since 2.4
 */
public class MessageFormatI18nMessageFormatter implements I18nMessageFormatter {

    @Override
    public String format(Locale locale, String message, Object... args) {
        MessageFormat formatter = new MessageFormat(message, locale);
        return formatter.format(args);
    }
}
