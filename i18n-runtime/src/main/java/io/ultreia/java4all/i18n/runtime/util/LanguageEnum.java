/*
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package io.ultreia.java4all.i18n.runtime.util;

import java.util.logging.Logger;

/**
 * Une énumération pour représenter le langue d'une locale définie dans la norme
 * <a href="http://www.iso.org/iso/iso_catalogue/catalogue_ics/catalogue_detail_ics.htm?csnumber=22109&ICS1=1&ICS2=140&ICS3=20"><code>ISO
 * 639-1:1998  (ICS n° 01.140.20)</code></a>.
 * <p>
 * <a href="http://www.loc.gov/standards/iso639-2/php/French_list.php">la liste
 * des codes</a>
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public enum LanguageEnum {

    aa, // Afar
    ab, // Abkhazian
    af, // Afrikaans
    am, // Amharic
    ar, // Arabic
    as, // Assamese
    ay, // Aymara
    az, // Azerbaijani
    ba, // Bashkir
    be, // Byelorussian
    bg, // Bulgarian
    bh, // Bihari
    bi, // Bislama
    bn, // Bengali; Bangla
    bo, // Tibetan
    br, // Breton
    ca, // Catalan
    co, // Corsican
    cs, // Czech
    cy, // Welsh
    da, // Danish
    de, // German
    dz, // Bhutani
    el, // Greek
    en, // English
    eo, // Esperanto
    es, // Spanish
    et, // Estonian
    eu, // Basque
    fa, // Persian
    fi, // Finnish
    fj, // Fiji
    fo, // Faroese
    fr, // French
    fy, // Frisian
    ga, // Irish
    gd, // Scots Gaelic
    gl, // Galician
    gn, // Guarani
    gu, // Gujarati
    ha, // Hausa
    he, // Hebrew (formerly iw)
    hi, // Hindi
    hr, // Croatian
    hu, // Hungarian
    hy, // Armenian
    ia, // Interlingua
    id, // Indonesian (formerly in)
    ie, // Interlingue
    ik, // Inupiak
    is, // Icelandic
    it, // Italian
    iu, // Inuktitut
    ja, // Japanese
    jw, // Javanese
    ka, // Georgian
    kk, // Kazakh
    kl, // Greenlandic
    km, // Cambodian
    kn, // Kannada
    ko, // Korean
    ks, // Kashmiri
    ku, // Kurdish
    ky, // Kirghiz
    la, // Latin
    ln, // Lingala
    lo, // Laothian
    lt, // Lithuanian
    lv, // Latvian, Lettish
    mg, // Malagasy
    mi, // Maori
    mk, // Macedonian
    ml, // Malayalam
    mn, // Mongolian
    mo, // Moldavian
    mr, // Marathi
    ms, // Malay
    mt, // Maltese
    my, // Burmese
    na, // Nauru
    ne, // Nepali
    nl, // Dutch
    no, // Norwegian
    oc, // Occitan
    om, // (Afan) Oromo
    or, // Oriya
    pa, // Punjabi
    pl, // Polish
    ps, // Pashto, Pushto
    pt, // Portuguese
    qu, // Quechua
    rm, // Rhaeto-Romance
    rn, // Kirundi
    ro, // Romanian
    ru, // Russian
    rw, // Kinyarwanda
    sa, // Sanskrit
    sd, // Sindhi
    sg, // Sangho
    sh, // Serbo-Croatian
    si, // Sinhalese
    sk, // Slovak
    sl, // Slovenian
    sm, // Samoan
    sn, // Shona
    so, // Somali
    sq, // Albanian
    sr, // Serbian
    ss, // Siswati
    st, // Sesotho
    su, // Sundanese
    sv, // Swedish
    sw, // Swahili
    ta, // Tamil
    te, // Telugu
    tg, // Tajik
    th, // Thai
    ti, // Tigrinya
    tk, // Turkmen
    tl, // Tagalog
    tn, // Setswana
    to, // Tonga
    tr, // Turkish
    ts, // Tsonga
    tt, // Tatar
    tw, // Twi
    ug, // Uighur
    uk, // Ukrainian
    ur, // Urdu
    uz, // Uzbek
    vi, // Vietnamese
    vo, // Volapuk
    wo, // Wolof
    xh, // Xhosa
    yi, // Yiddish (formerly ji)
    yo, // Yoruba
    za, // Zhuang
    zh, // Chinese
    zu; // Zulu

    public static LanguageEnum valueOf(String language,
                                       LanguageEnum defaultValue) {
        LanguageEnum languageValue = null;
        try {
            languageValue = valueOf(language.toLowerCase());
        } catch (Exception e) {
            Logger.getAnonymousLogger().warning(
                    "Unfound language " + language +
                            ", will use default one : " + defaultValue
            );
        }
        return languageValue == null ? defaultValue : languageValue;
    }
}
