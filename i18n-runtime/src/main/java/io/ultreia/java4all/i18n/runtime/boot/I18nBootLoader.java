package io.ultreia.java4all.i18n.runtime.boot;

/*-
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;

import java.util.Locale;

/**
 * Created by tchemit on 05/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public interface I18nBootLoader {

    I18nBootLoader DEFAULT_BOOT_LOADER = new DefaultI18nBootLoader(I18nConfiguration.DEFAULT_CONFIGURATION);

    I18nConfiguration getConfiguration();

    I18nLanguageProvider init(Locale locale) throws I18nInitializationException;

}
