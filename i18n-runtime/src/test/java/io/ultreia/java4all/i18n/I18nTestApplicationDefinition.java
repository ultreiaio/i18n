package io.ultreia.java4all.i18n;

/*-
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.i18n.spi.I18nApplicationDefinition;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

/**
 * Created by tchemit on 06/11/2018.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@AutoService(I18nApplicationDefinition.class)
public class I18nTestApplicationDefinition extends I18nApplicationDefinition {

    public I18nTestApplicationDefinition() {
        super("io.ultreia.java4all.i18n", "I18nTest", StandardCharsets.UTF_8, "1.0",
              new String[]{
                      "en_GB","fr_FR","ja_JP"
              },
              Collections.emptyMap(),
              new String[]{
                      "io.ultreia.java4all.i18n__I18nTest__en_GB__fr_FR__ja_JP"
              }, new String[]{}, new String[]{});
    }
}
