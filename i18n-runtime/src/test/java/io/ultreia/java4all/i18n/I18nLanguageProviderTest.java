/*
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package io.ultreia.java4all.i18n;

import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;

/**
 * FIXME
 * Tests {@link I18nLanguageProvider}.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.1
 */
public class I18nLanguageProviderTest {

//    String encoding;
//
//    Locale locale;
//
//    I18nLanguage language;
//
//    I18nLanguageProvider store;
//
//
//    static DefaultI18nInitializer initializer;
//
//    @BeforeClass
//    public static void beforeClass() {
//
//        initializer = new DefaultI18nInitializer(
//                I18nLanguageProviderTest.class.getSimpleName());
//    }
//
//    @After
//    public void after() throws Exception {
//        I18n.close();
//    }
//
//    @Test
//    public void testGetLoader() throws Exception {
//
//        Assert.assertNull(I18n.store);
//
//        store = I18n.getStore();
//        Assert.assertNull(I18n.store);
//
//        I18n.init(initializer, null);
//
//        store = I18n.getStore();
//
//        Assert.assertNotNull(store);
//        //assertEquals(I18n.DEFAULT_ENCODING, store.getEncoding());
//        Assert.assertNotNull(store.getCurrentLanguage());
//    }
//
//    @Test
//    public void testChangeLocale() throws Exception {
//
//        I18n.init(initializer, locale = I18nLocaleHelper.newLocale("fr_FR"));
////        locale = I18nUtil.newLocale("fr_FR");
////        encoding = I18n.ISO_8859_1_ENCONDING;
//        updateLanguage();
//        assertNbLanguages(1);
//        updateLanguage();
//        assertNbLanguages(1);
//
//        locale = I18nLocaleHelper.newLocale("en_GB");
//        updateLanguage();
//        assertLanguageChanged();
//        assertNbLanguages(2);
//
//        locale = I18nLocaleHelper.newLocale("en_US");
//        updateLanguage();
//        assertLanguageChanged();
//        assertNbLanguages(3);
//
//        locale = I18nLocaleHelper.newLocale("en");
//        updateLanguage();
//        assertLanguageChanged();
//        assertNbLanguages(4);
//    }
//
//    protected void assertLanguageChanged() {
//        Assert.assertNotSame(language, store.getCurrentLanguage());
//    }
//
//    protected void assertNbLanguages(int i) {
//        Assert.assertEquals(i, store.getLanguages().length);
//    }
//
//    protected void updateLanguage() {
//        language = store == null ? null : store.getCurrentLanguage();
//        store = I18n.getStore();
//        store.setCurrentLocale(locale);
//    }

}

