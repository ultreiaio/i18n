/*
 * #%L
 * I18n :: Runtime
 * %%
 * Copyright (C) 2018 - 2024 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package io.ultreia.java4all.i18n;

import io.ultreia.java4all.i18n.runtime.I18nConfiguration;
import io.ultreia.java4all.i18n.runtime.I18nLanguageProvider;
import io.ultreia.java4all.i18n.runtime.boot.DefaultI18nBootLoader;
import io.ultreia.java4all.i18n.runtime.format.MessageFormatI18nMessageFormatter;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.text.MessageFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * @author Tony Chemit - dev@tchemit.fr
 */
public class I18nTest {

    private static Locale defaultLocale;
    private DefaultI18nBootLoader initializer;

    @BeforeClass
    public static void before() {
        defaultLocale = Locale.getDefault();
    }

    @Before
    public void beforeClass() {
        I18n.close();
        initializer = new DefaultI18nBootLoader(I18nConfiguration.createDefaultConfiguration());
    }

    @After
    public void after() {
        I18n.close();
    }

    @Test
    public void testWithNoInit() {

        String expected;
        String actual;

        // pas de traduction possible car l'initialiser n'est pas bon :)
        // FAUX! Depuis la version 4.0 ça se charge tout seul :X
        expected = "key.with.param";
        actual = I18n.t("key.with.param");
        I18nLanguageProvider languageProvider = I18n.getLanguageProvider();
        if (
                languageProvider.getApplicationDefinition().getLocales().contains(defaultLocale)
                        || languageProvider.getApplicationDefinition().getLocales().contains(languageProvider.getLocale(defaultLocale))
        ) {
            Assert.assertNotEquals(expected, actual);
        } else {
            Assert.assertEquals(expected, actual);
        }

    }

    @Test
    public void testWithNoInit2() {


        String expected;
        String actual;

        // pas de traduction possible car l'initialiser n'est pas bon :)
        // FAUX! Depuis la version 4.0 ça se charge tout seul :X

        expected = "key.with.param";
        actual = I18n.l(Locale.FRANCE, "key.with.param");
        Assert.assertNotEquals(expected, actual);

        actual = I18n.l(Locale.UK, "key.with.param");
        Assert.assertNotEquals(expected, actual);

    }


    @Test
    public void testDefaultInit() {

        Assert.assertNull(I18n.getLanguageProvider());

        I18n.init(null, null);

        Assert.assertNotNull(I18n.getLanguageProvider());
        Assert.assertNotNull(I18n.getDefaultLocale());
        Assert.assertEquals(Locale.getDefault().getLanguage(), I18n.getDefaultLocale().getLanguage());
        Assert.assertNotNull(I18n.getLanguageProvider().getCurrentLanguage());
        Assert.assertEquals(Locale.getDefault().getLanguage(), I18n.getLanguageProvider().getCurrentLocale().getLanguage());

    }

    @Test
    public void testJapanese() {

        I18n.init(initializer, Locale.JAPAN);

        Assert.assertNotNull(I18n.getLanguageProvider().getConfiguration().getEncoding());
        Assert.assertEquals("最初の", I18n.t("key.one"));
    }

    @Test
    public void testSimple() {

        String expected;
        String actual;

        // passage en français

        I18n.init(initializer, Locale.FRANCE);
//        I18n.getStore().setLanguage(Locale.FRANCE);

        expected = "Clé avec %s";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Clé avec param";
        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // passage en anglais

        I18n.setDefaultLocale(Locale.UK);
//        I18n.getStore().setLanguage(Locale.UK);

        expected = "Key with %s";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Key with param";
        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // passage langue inconnue

        I18n.setDefaultLocale(Locale.CHINA);
//        I18n.getStore().setLanguage(Locale.CHINA);

        expected = "key.with.param";
        actual = I18n.t("key.with.param");
        Assert.assertEquals(expected, actual);

        actual = I18n.t("key.with.param", "param");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMultiLanguage() {

        String expected;
        String actual;

        // en français

        I18n.init(initializer, null);

        expected = "Clé avec %s";
        actual = I18n.l(Locale.FRANCE, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Clé avec param";
        actual = I18n.l(Locale.FRANCE, "key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // en anglais

//        I18n.getStore().setLanguage(Locale.UK);

        expected = "Key with %s";
        actual = I18n.l(Locale.UK, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "Key with param";
        actual = I18n.l(Locale.UK, "key.with.param", "param");
        Assert.assertEquals(expected, actual);

        // dans une langue inconnue

        expected = "key.with.param";
        actual = I18n.l(Locale.CHINA, "key.with.param");
        Assert.assertEquals(expected, actual);

        expected = "key.with.param";
        actual = I18n.l(Locale.CHINA, "key.with.param", "param");
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testFormatterMessageFormat() {

        initializer.getConfiguration().setMessageFormatter(new MessageFormatI18nMessageFormatter());
        I18n.init(initializer, Locale.UK);

        Date date = new GregorianCalendar(2011, Calendar.MAY, 5).getTime();

        String expected = new MessageFormat("Key with {0,date}", Locale.UK).format(new Object[]{date});
        // FIXME With java 8 use this: String expected = "Key with 5-May-2011";
        // FIXME With java 10 use this: String expected = "Key with 5 May 2011";
        String actual = I18n.l(Locale.UK, "key.with.date", date);
        Assert.assertEquals(expected, actual);

        expected = "とキー 2011/05/05";
        actual = I18n.l(Locale.JAPAN, "key.with.date", date);
        Assert.assertEquals(expected, actual);

        expected = "Clé avec 5 mai 2011";
        actual = I18n.l(Locale.FRANCE, "key.with.date", date);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testMissingKeyReturnNull() {

        initializer.getConfiguration().setMissingKeyReturnNull(false);
        I18n.init(initializer, Locale.FRANCE);

        String key = "youhou";
        String text = I18n.t(key);
        Assert.assertNotNull(text);
        Assert.assertEquals(key, text);

        initializer.getConfiguration().setMissingKeyReturnNull(true);
        I18n.init(initializer, Locale.FRANCE);

        text = I18n.t(key);
        Assert.assertNull(text);

        text = I18n.t(key, 123);
        Assert.assertNull(text);
    }

    @Test
    public void testHasKey() {

        I18n.init(initializer, Locale.FRANCE);

        Assert.assertFalse(I18n.hasKey("key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey("key.fr.only.empty"));
        Assert.assertTrue(I18n.hasKey("key.fr.only.filled"));

        Assert.assertFalse(I18n.hasKey(Locale.FRANCE, "key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey(Locale.FRANCE, "key.fr.only.empty"));
        Assert.assertTrue(I18n.hasKey(Locale.FRANCE, "key.fr.only.filled"));

        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.missing"));
        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.empty"));
        Assert.assertFalse(I18n.hasKey(Locale.JAPAN, "key.fr.only.filled"));

    }

}
