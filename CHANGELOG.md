# I18n changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-02-13 13:57.

## Version [4.0-beta-27](https://gitlab.com/ultreiaio/i18n/-/milestones/75)

**Closed at 2024-02-13.**


### Issues
  * [[Evolution 238]](https://gitlab.com/ultreiaio/i18n/-/issues/238) **Remove org.nuiton.i18n package from i18n-runtime module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 239]](https://gitlab.com/ultreiaio/i18n/-/issues/239) **Remove module i18n-api (not used at all from dark ages...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 240]](https://gitlab.com/ultreiaio/i18n/-/issues/240) **Remove all the xml based i18n parsers** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 241]](https://gitlab.com/ultreiaio/i18n/-/issues/241) **Move i18n-editor to JAXX project (since JAXX use i18n)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-26](https://gitlab.com/ultreiaio/i18n/-/milestones/74)

**Closed at 2024-02-13.**


### Issues
  * [[Evolution 237]](https://gitlab.com/ultreiaio/i18n/-/issues/237) **Remove nuiton-converter dependency** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-25](https://gitlab.com/ultreiaio/i18n/-/milestones/73)

**Closed at 2023-06-27.**


### Issues
  * [[Evolution 236]](https://gitlab.com/ultreiaio/i18n/-/issues/236) **In I18n editor, when user ask to quit and the model was modified, ask him if he does want to perform an export** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-24](https://gitlab.com/ultreiaio/i18n/-/milestones/72)

**Closed at 2022-05-18.**


### Issues
  * [[Evolution 234]](https://gitlab.com/ultreiaio/i18n/-/issues/234) **Replace processor option to get module i18n configuration (use a generated file by init mojo)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 235]](https://gitlab.com/ultreiaio/i18n/-/issues/235) **Improve how to skip already processed annotations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-23](https://gitlab.com/ultreiaio/i18n/-/milestones/71)

**Closed at 2021-12-29.**


### Issues
  * [[Evolution 233]](https://gitlab.com/ultreiaio/i18n/-/issues/233) **Improve generate-i18n-labels mojo to be able to it for the same maven module where types are.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-22](https://gitlab.com/ultreiaio/i18n/-/milestones/70)

**Closed at 2021-12-12.**


### Issues
No issue.

## Version [4.0-beta-20](https://gitlab.com/ultreiaio/i18n/-/milestones/69)

**Closed at 2021-11-13.**


### Issues
  * [[Evolution 232]](https://gitlab.com/ultreiaio/i18n/-/issues/232) **Replace nuiton-utils and nuiton-version by java-util** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-19](https://gitlab.com/ultreiaio/i18n/-/milestones/68)

**Closed at 2021-09-14.**


### Issues
  * [[Evolution 230]](https://gitlab.com/ultreiaio/i18n/-/issues/230) **Simplify generated java code form I18n enumeration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 231]](https://gitlab.com/ultreiaio/i18n/-/issues/231) **Remove parse-java-enumerations mojo (replace by annotations TranslateEnumeration and TranslateEnumerations)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-18](https://gitlab.com/ultreiaio/i18n/-/milestones/67)

**Closed at 2021-09-06.**


### Issues
  * [[Evolution 228]](https://gitlab.com/ultreiaio/i18n/-/issues/228) **Review how to deal with enumerations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 229]](https://gitlab.com/ultreiaio/i18n/-/issues/229) **Introduce copy-module-resources mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-17](https://gitlab.com/ultreiaio/i18n/-/milestones/66)

**Closed at 2021-08-17.**


### Issues
  * [[Evolution 227]](https://gitlab.com/ultreiaio/i18n/-/issues/227) **Make BeanPropertyI18nKeyProducerSupport use new labels mapping** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-16](https://gitlab.com/ultreiaio/i18n/-/milestones/65)

**Closed at 2021-08-12.**


### Issues
  * [[Evolution 224]](https://gitlab.com/ultreiaio/i18n/-/issues/224) **Rename RegisterI18nLabels to RegisterI18nLabel and introduce RegisterI18nLabels annotation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 225]](https://gitlab.com/ultreiaio/i18n/-/issues/225) **Make generate-i18n-labels generate-resource default phase** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 226]](https://gitlab.com/ultreiaio/i18n/-/issues/226) **Introduce DefaultBeanPropertyI18nKeyProducer using the generated labels mapping with the new mojo goal** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-15](https://gitlab.com/ultreiaio/i18n/-/milestones/64)

**Closed at 2021-08-08.**


### Issues
  * [[Evolution 220]](https://gitlab.com/ultreiaio/i18n/-/issues/220) **Introduce TypeTranslator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 221]](https://gitlab.com/ultreiaio/i18n/-/issues/221) **Introduce RegisterI18nLabels annotation to compute smart I18n labels** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 222]](https://gitlab.com/ultreiaio/i18n/-/issues/222) **Introduce generate-i18n-labels mojo to generate i18n labels using RegisterI18nLabels annotation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 223]](https://gitlab.com/ultreiaio/i18n/-/issues/223) **Be able to force multiple execution of a mojo on a same maven module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-14](https://gitlab.com/ultreiaio/i18n/-/milestones/63)

**Closed at 2020-06-19.**


### Issues
  * [[Anomalie 219]](https://gitlab.com/ultreiaio/i18n/-/issues/219) **In generate mojo, using strictMode is no more effective** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-13](https://gitlab.com/ultreiaio/i18n/-/milestones/62)

**Closed at 2020-03-22.**


### Issues
  * [[Evolution 218]](https://gitlab.com/ultreiaio/i18n/-/issues/218) **Reuse fork of antlr4 (ten times speed...)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-12](https://gitlab.com/ultreiaio/i18n/-/milestones/61)

**Closed at 2020-03-22.**


### Issues
  * [[Evolution 216]](https://gitlab.com/ultreiaio/i18n/-/issues/216) **Migrates to java 10, but still stay on java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 217]](https://gitlab.com/ultreiaio/i18n/-/issues/217) **Add template when generate application module** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-11](https://gitlab.com/ultreiaio/i18n/-/milestones/60)

**Closed at 2019-10-21.**


### Issues
  * [[Anomalie 150]](https://gitlab.com/ultreiaio/i18n/-/issues/150) **NullPointerException during parserJava** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 215]](https://gitlab.com/ultreiaio/i18n/-/issues/215) **New threading issue** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-10](https://gitlab.com/ultreiaio/i18n/-/milestones/59)

**Closed at 2019-10-18.**


### Issues
  * [[Evolution 214]](https://gitlab.com/ultreiaio/i18n/-/issues/214) **Make mojo thread safe** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 213]](https://gitlab.com/ultreiaio/i18n/-/issues/213) **Error while parsing java files** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-9](https://gitlab.com/ultreiaio/i18n/-/milestones/58)

**Closed at 2019-06-21.**


### Issues
  * [[Evolution 212]](https://gitlab.com/ultreiaio/i18n/-/issues/212) **Improve BeanPropertyI18nKeyProducer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-8](https://gitlab.com/ultreiaio/i18n/-/milestones/57)

**Closed at 2018-11-19.**


### Issues
  * [[Anomalie 211]](https://gitlab.com/ultreiaio/i18n/-/issues/211) **Fix usage of new groupId separator :(** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-7](https://gitlab.com/ultreiaio/i18n/-/milestones/56)

**Closed at 2018-11-19.**


### Issues
  * [[Evolution 209]](https://gitlab.com/ultreiaio/i18n/-/issues/209) **Move i18n-editor to io.ultreia.java4all.i18n package** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 210]](https://gitlab.com/ultreiaio/i18n/-/issues/210) **Change the separator between groupId and artifactId in generated files (does not work on Windows)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-6](https://gitlab.com/ultreiaio/i18n/-/milestones/55)

**Closed at 2018-11-18.**


### Issues
  * [[Evolution 207]](https://gitlab.com/ultreiaio/i18n/-/issues/207) **Improve I18n to be able to use languages using promotion if the language does not exist in store** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 208]](https://gitlab.com/ultreiaio/i18n/-/issues/208) **Deprecates all old api from nuiton.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-5](https://gitlab.com/ultreiaio/i18n/-/milestones/54)

**Closed at 2018-11-16.**


### Issues
  * [[Evolution 206]](https://gitlab.com/ultreiaio/i18n/-/issues/206) **Improve how to store i18n editor exports** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-4](https://gitlab.com/ultreiaio/i18n/-/milestones/53)

**Closed at 2018-11-14.**


### Issues
  * [[Evolution 205]](https://gitlab.com/ultreiaio/i18n/-/issues/205) **Improve i18n-editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-3](https://gitlab.com/ultreiaio/i18n/-/milestones/52)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 203]](https://gitlab.com/ultreiaio/i18n/-/issues/203) **Fix UserI18nBootLoader (using bad directories)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 204]](https://gitlab.com/ultreiaio/i18n/-/issues/204) **application-bundle mojo generates bad ApplicationDefinition file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-2](https://gitlab.com/ultreiaio/i18n/-/milestones/51)

**Closed at *In progress*.**


### Issues
  * [[Evolution 201]](https://gitlab.com/ultreiaio/i18n/-/issues/201) **Make templateExtension configurable in module and application I18n definition** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 202]](https://gitlab.com/ultreiaio/i18n/-/issues/202) **Improve where to store i18n files in class-path** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-beta-1](https://gitlab.com/ultreiaio/i18n/-/milestones/50)

**Closed at *In progress*.**


### Issues
  * [[Evolution 198]](https://gitlab.com/ultreiaio/i18n/-/issues/198) **Propose a new way of dealing with i18n keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 199]](https://gitlab.com/ultreiaio/i18n/-/issues/199) **Remove deprecated BundleMojo.failsIfWarning** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 200]](https://gitlab.com/ultreiaio/i18n/-/issues/200) **Replace i18n-api by i18n-runtime** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-alpha-15](https://gitlab.com/ultreiaio/i18n/-/milestones/49)

**Closed at *In progress*.**


### Issues
  * [[Evolution 194]](https://gitlab.com/ultreiaio/i18n/-/issues/194) **Introduce BeanPropertyI18nKeyProducer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 195]](https://gitlab.com/ultreiaio/i18n/-/issues/195) **Add categories in I18n editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 196]](https://gitlab.com/ultreiaio/i18n/-/issues/196) **Add search feature in I18n editor** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 197]](https://gitlab.com/ultreiaio/i18n/-/issues/197) **Add count of data in I18n editor in any tabs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-alpha-14](https://gitlab.com/ultreiaio/i18n/-/milestones/48)

**Closed at *In progress*.**


### Issues
No issue.

## Version [4.0-alpha-13](https://gitlab.com/ultreiaio/i18n/-/milestones/47)

**Closed at 2018-08-02.**


### Issues
  * [[Tâche 193]](https://gitlab.com/ultreiaio/i18n/-/issues/193) **Improve Log4J configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-alpha-12](https://gitlab.com/ultreiaio/i18n/-/milestones/46)

**Closed at 2018-07-31.**


### Issues
  * [[Tâche 192]](https://gitlab.com/ultreiaio/i18n/-/issues/192) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-alpha-11](https://gitlab.com/ultreiaio/i18n/-/milestones/45)

**Closed at 2018-03-19.**


### Issues
  * [[Evolution 189]](https://gitlab.com/ultreiaio/i18n/-/issues/189) **Be able to have templates in classpath** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 190]](https://gitlab.com/ultreiaio/i18n/-/issues/190) **While using templates in editor, can resolve template resources for default locale** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 191]](https://gitlab.com/ultreiaio/i18n/-/issues/191) **Avoid to execute mojo more than once** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [4.0-alpha-10](https://gitlab.com/ultreiaio/i18n/-/milestones/43)

**Closed at *In progress*.**


### Issues
  * [[Anomalie 185]](https://gitlab.com/ultreiaio/i18n/-/issues/185) **Sometimes while parsing, there is somewhere a deadlock** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 184]](https://gitlab.com/ultreiaio/i18n/-/issues/184) **Use java-util dependency (replace nuiton-utils)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 186]](https://gitlab.com/ultreiaio/i18n/-/issues/186) **Fix usage of deprecated API (antlr)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 187]](https://gitlab.com/ultreiaio/i18n/-/issues/187) **Remove LocaleConverter** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 188]](https://gitlab.com/ultreiaio/i18n/-/issues/188) **Remove I18nDefaultTooltipFilter** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.6.3](https://gitlab.com/ultreiaio/i18n/-/milestones/33)
&#10;&#10;*(from redmine: created on 2016-09-07)*

**Closed at 2017-02-09.**


### Issues
  * [[Evolution 151]](https://gitlab.com/ultreiaio/i18n/-/issues/151) **Do not use ConverterUtils, cause some linkage error in some special cases** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 152]](https://gitlab.com/ultreiaio/i18n/-/issues/152) **Update dependencies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.6.2](https://gitlab.com/ultreiaio/i18n/-/milestones/32)
&#10;&#10;*(from redmine: created on 2016-09-02)*

**Closed at 2016-09-07.**


### Issues
  * [[Anomalie 149]](https://gitlab.com/ultreiaio/i18n/-/issues/149) **GenerateI18nHelper does not work if enum has abstract methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.6.1](https://gitlab.com/ultreiaio/i18n/-/milestones/31)
&#10;&#10;*(from redmine: created on 2016-08-28)*

**Closed at 2016-09-02.**


### Issues
  * [[Evolution 147]](https://gitlab.com/ultreiaio/i18n/-/issues/147) **Improve generateI18nEnumHelper by adding a way to generate also a description i18n key** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 148]](https://gitlab.com/ultreiaio/i18n/-/issues/148) **Be able to generate enums i18n keys even if enum is not still compiled** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 146]](https://gitlab.com/ultreiaio/i18n/-/issues/146) **The generate mojo should only fails (using failsIfWarning) if there is some missing key values but not if there is some missing key** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.6](https://gitlab.com/ultreiaio/i18n/-/milestones/30)
&#10;&#10;*(from redmine: created on 2016-08-28)*

**Closed at 2016-08-28.**


### Issues
  * [[Evolution 145]](https://gitlab.com/ultreiaio/i18n/-/issues/145) **Introduce a new mojo : generateEnumKeys to generate enum i18n keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.5.2](https://gitlab.com/ultreiaio/i18n/-/milestones/29)
&#10;&#10;*(from redmine: created on 2016-08-28)*

**Closed at 2016-08-28.**


### Issues
  * [[Evolution 144]](https://gitlab.com/ultreiaio/i18n/-/issues/144) **Improve failsIfExist flag for multi-locales builds on gen mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.5.1](https://gitlab.com/ultreiaio/i18n/-/milestones/27)
&#10;&#10;*(from redmine: created on 2016-03-14)*

**Closed at 2016-08-28.**


### Issues
  * [[Evolution 143]](https://gitlab.com/ultreiaio/i18n/-/issues/143) **Add failsIfWarning on gen mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.5](https://gitlab.com/ultreiaio/i18n/-/milestones/28)
&#10;&#10;*(from redmine: created on 2016-04-25)*

**Closed at 2016-05-02.**


### Issues
  * [[Evolution 142]](https://gitlab.com/ultreiaio/i18n/-/issues/142) **Introduce a goal to check bundles integrity** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.4.1](https://gitlab.com/ultreiaio/i18n/-/milestones/26)
&#10;&#10;*(from redmine: created on 2016-03-03)*

**Closed at 2016-03-03.**


### Issues
  * [[Evolution 140]](https://gitlab.com/ultreiaio/i18n/-/issues/140) **Update nuitonpom to 9 and set correct java level** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.4](https://gitlab.com/ultreiaio/i18n/-/milestones/24)
&#10;&#10;*(from redmine: created on 2014-08-01)*

**Closed at 2016-02-29.**


### Issues
  * [[Evolution 136]](https://gitlab.com/ultreiaio/i18n/-/issues/136) **Use java 8 API to scan code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 138]](https://gitlab.com/ultreiaio/i18n/-/issues/138) **Use maven 3 API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 139]](https://gitlab.com/ultreiaio/i18n/-/issues/139) **Clean javadoc for java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 135]](https://gitlab.com/ultreiaio/i18n/-/issues/135) **Fail to extract t( t( &quot;string&quot;) )** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 137]](https://gitlab.com/ultreiaio/i18n/-/issues/137) **Update libs** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.3](https://gitlab.com/ultreiaio/i18n/-/milestones/23)
&#10;&#10;*(from redmine: created on 2014-07-23)*

**Closed at 2014-08-01.**


### Issues
  * [[Evolution 107]](https://gitlab.com/ultreiaio/i18n/-/issues/107) **Add a mojo to merge back csv generated file to incoming projects** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 130]](https://gitlab.com/ultreiaio/i18n/-/issues/130) **Review site** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 131]](https://gitlab.com/ultreiaio/i18n/-/issues/131) **Review the csv bundle mojos** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 132]](https://gitlab.com/ultreiaio/i18n/-/issues/132) **Remove deprecated Tapestry mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 133]](https://gitlab.com/ultreiaio/i18n/-/issues/133) **Sanity mojo parameters** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.2](https://gitlab.com/ultreiaio/i18n/-/milestones/22)
&#10;&#10;*(from redmine: created on 2014-05-14)*

**Closed at 2014-07-23.**


### Issues
  * [[Evolution 126]](https://gitlab.com/ultreiaio/i18n/-/issues/126) **Only parse i18n java files when found a **org.nuiton.i18n.I18n** pattern inside it** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 127]](https://gitlab.com/ultreiaio/i18n/-/issues/127) **Deprecates converter APi and use now nuiton-converter instead** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 90]](https://gitlab.com/ultreiaio/i18n/-/issues/90) **I18n doesn&#39;t work on windows** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 128]](https://gitlab.com/ultreiaio/i18n/-/issues/128) **Use nuitonpom and the new site layout** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 129]](https://gitlab.com/ultreiaio/i18n/-/issues/129) **Migrates to Git** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.1](https://gitlab.com/ultreiaio/i18n/-/milestones/20)
&#10;&#10;*(from redmine: created on 2014-02-04)*

**Closed at 2014-05-07.**


### Issues
  * [[Tâche 124]](https://gitlab.com/ultreiaio/i18n/-/issues/124) **Keep deprecated methods _, l_ and n_ as you can still compile code withit in Java SE 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 125]](https://gitlab.com/ultreiaio/i18n/-/issues/125) **Updates mavenpom to 5.0** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0](https://gitlab.com/ultreiaio/i18n/-/milestones/10)
Java 1.8 compatibility&#10;&#10;*(from redmine: created on 2011-01-04)*

**Closed at 2014-02-04.**


### Issues
  * [[Evolution 85]](https://gitlab.com/ultreiaio/i18n/-/issues/85) **Add new AST parser for java file** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Evolution 110]](https://gitlab.com/ultreiaio/i18n/-/issues/110) **Rename I18n methods to be compatible with jre 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Anomalie 83]](https://gitlab.com/ultreiaio/i18n/-/issues/83) **Commented keys are parsed in GWT parser mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 118]](https://gitlab.com/ultreiaio/i18n/-/issues/118) **Update to commons-collections4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 119]](https://gitlab.com/ultreiaio/i18n/-/issues/119) **Remove deprecated parser (jsp and tapestry)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 120]](https://gitlab.com/ultreiaio/i18n/-/issues/120) **Updates plexus-utils to 3.0.17** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 121]](https://gitlab.com/ultreiaio/i18n/-/issues/121) **Updates xwork-core to 2.3.16** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Tâche 122]](https://gitlab.com/ultreiaio/i18n/-/issues/122) **Updates mavenpom to 4.7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

